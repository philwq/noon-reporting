
package com.fulcrummaritime.noonreporting;

// import android.os.Build;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.fulcrummaritime.noonreporting.reports.ArrivalReportActivity;
import com.fulcrummaritime.noonreporting.reports.BerthingReportActivity;
import com.fulcrummaritime.noonreporting.reports.DepartureReportActivity;
import com.fulcrummaritime.noonreporting.reports.DuringLoadingReportActivity;
import com.fulcrummaritime.noonreporting.reports.WhileAtPortReportActivity;

// import android.widget.Toast;

// import android_serialport_api.SerialPortWrapper;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        final MainActivity baseActivity = this;

        SerialPortWrapper serialPortReadWrapper = new SerialPortWrapper("/dev/tty" + (Build.MODEL.toLowerCase().indexOf("custom", 0) > -1 ? "S" : "O") + "0") {
            @Override
            protected void onDataReceived(final String payload) {
                super.onDataReceived(payload);
                baseActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(baseActivity, payload, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };

        serialPortReadWrapper.startReading();
        */
    }

    public void arrivalMenuTitleOnClick(View view) {
        startActivity(new Intent(this, ArrivalReportActivity.class));
    }

    public void departureMenuTitleOnClick(View view) {
        startActivity(new Intent(this, DepartureReportActivity.class));
    }

    public void duringVoyageMenuTitleOnClick(View view) {
        commonMenuTitleOnClick(view);
    }

    public void whileAtPortMenuTitleOnClick(View view) {
        startActivity(new Intent(this, WhileAtPortReportActivity.class));
    }

    public void berthingMenuTitleOnClick(View view) {
        startActivity(new Intent(this, BerthingReportActivity.class));
    }

    public void duringLoadingMenuTitleOnClick(View view) {
        startActivity(new Intent(this, DuringLoadingReportActivity.class));
    }

    private void commonMenuTitleOnClick(View view) {
        new Shared(this).showAlert(((Button)view).getText() + " - future enhancement.", this, false, true);
    }
}
