
package com.fulcrummaritime.noonreporting;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

import android_serialport_api.SerialPortFinder;
import android_serialport_api.SerialPortWrapper;

public class Shared {

    private final Activity context;

    public Shared(final AppCompatActivity context) {
        this.context = context;
    }

    public void showAlertNoTitle(String msg, final Activity context, final boolean doFinish) {

    }

    public void showAlert(String msg, final Activity context, final boolean doFinish, boolean showTitle) {

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        if (showTitle)
            alert.setTitle(context.getTitle());
        alert.setMessage(msg);
        alert.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (doFinish) {
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                }
            }
        });

        alert.show();
    }

    public void showAlert(String msg, boolean doFinish, boolean showTitle) {
        showAlert(msg, this.context, doFinish, showTitle);
    }

    public String getPayloadForSend(List<String> speciesSelected) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < speciesSelected.size(); i++) {
            String itm = speciesSelected.get(i);
            sb.append(itm.substring(itm.lastIndexOf("-") + 2)).append(i == speciesSelected.size() - 1 ? "" : "&");

        }
        return sb.toString();
    }

    /*
    public Map<String, ArrayList<String>> getSeparatedSpeciesWeightListsFromPayload(String payload, Context context) {
        String[] species = payload.split("&");

        Map<String, ArrayList<String>> combined = new HashMap<>();
        ArrayList<String> speciesList = new ArrayList<>();
        ArrayList<String> weightList = new ArrayList<>();

        for (String speciesWithWeight :
                species) {
            String[] fishSplit = speciesWithWeight.split("=");
            speciesList.add(getFullSpeciesName(fishSplit[0], context));
            weightList.add(fishSplit[1]);
        }

        combined.put("species", speciesList);
        combined.put("weight", weightList);

        return combined;

    }
    */

    /*
    private String getFullSpeciesName(String abbreviatedName, Context context) {

        if (fishMap == null) {
            String[] fishList = context.getResources().getStringArray(R.array.species_list);
            fishMap = new HashMap<>();
            for (String combined :
                    fishList) {
                String fishSplit[] = combined.split("-");
                fishMap.put(fishSplit[1].trim(), combined.trim());
            }

        }

        return fishMap.get(abbreviatedName);
    }
    */

    public void sendPayload(final AppCompatActivity context, String payload) {
        SerialPortWrapper serialPortWrapper = new SerialPortWrapper("/dev/tty" + (Build.MODEL.toLowerCase().indexOf("custom", 0) > -1 ? "S" : "O") + "0");
        if (serialPortWrapper.writeOnce(payload)) {
            showAlert(context.getString(R.string.report_successfully_sent), context, true, true);
        } else {
            showAlert(String.format(context.getString(R.string.report_not_sent), context.getString(serialPortWrapper.errorId)), context, true, true);
        }
    }

    public void serialPortLogger() {
        String[] allDevices = (new SerialPortFinder()).getAllDevicesPath();
        for (String device : allDevices)
            Log.d("serialPortLogger()", device);
    }

    public void activityOnBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        alert.setTitle(context.getTitle());
        alert.setMessage(context.getString(R.string.activity_leave_confirmation_text));

        alert.setPositiveButton(R.string.activity_leave_confirmation_positive_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.finish();
            }
        });

        alert.setNegativeButton(R.string.activity_leave_confirmation_negative_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alert.show();
    }
}
