
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.TimePickerWithTitleFragment;

public class FinishWithEnginesTime extends BaseItem {

    public final String FIELD_CODE = "FE";
    private Context context;
    TimePickerWithTitleFragment rootItem;

    public FinishWithEnginesTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = TimePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.finish_with_engines_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getTimeAsHHMM();
    }
}