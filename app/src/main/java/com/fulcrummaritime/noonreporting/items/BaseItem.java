
package com.fulcrummaritime.noonreporting.items;

public class BaseItem {
    protected String shadowValue = "";
    public BaseItem() { }
    public boolean isDirty(String itemValue) {
        return !itemValue.equalsIgnoreCase(shadowValue);
    }
}