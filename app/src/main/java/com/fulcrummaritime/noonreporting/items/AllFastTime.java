
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.AutoText;

import com.fulcrummaritime.noonreporting.R;

import layout.TimePickerWithTitleFragment;

public class AllFastTime extends BaseItem {

    public final String FIELD_CODE = "AF";
    private Context context;
    TimePickerWithTitleFragment rootItem;

    public AllFastTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = TimePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.all_fast_label_text));
        // Some (date time) items are auto initialised, also may add a constructor param.
        // to provide additional functionality. Either way keep track of item change to
        // allow prompt if backing out of report.
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getTimeAsHHMM();
    }
}