
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.InputType;

import com.fulcrummaritime.noonreporting.R;

import layout.EditTextWithTitleFragment;

public class NextPortEstimatedDraftAft extends BaseItem {

    public final String FIELD_CODE = "AP";
    private Context context;
    EditTextWithTitleFragment rootItem;

    public NextPortEstimatedDraftAft(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = EditTextWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.next_port_estimated_draft_aft_label_text));
        rootItem.setInputType(InputType.TYPE_CLASS_NUMBER);
        rootItem.setWidth(53);
        rootItem.setMaxLength(2);
        shadowValue = getValue();
    }

    public void setText(String text) {
        rootItem.setText(text);
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.next_port_estimated_draft_aft_error_text));
    }

    public String getValue() {
        return rootItem.getValueAsString();
    }
}
