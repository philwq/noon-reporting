
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.InputType;

import com.fulcrummaritime.noonreporting.R;

import layout.EditTextWithTitleFragment;

public class TotalROBHSFuelOilRME extends BaseItem {

    public final String FIELD_CODE = "HO";
    private Context context;
    EditTextWithTitleFragment rootItem;

    public TotalROBHSFuelOilRME(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = EditTextWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.total_rob_hs_fuel_oil_rme_label_text));
        rootItem.setInputType(InputType.TYPE_CLASS_NUMBER);
        rootItem.setWidth(66);
        rootItem.setMaxLength(4);
        shadowValue = getValue();
    }

    public void setText(String text) {
        rootItem.setText(text);
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.total_rob_hs_fuel_oil_rme_error_text));
    }

    public String getValue() {
        return rootItem.getValueAsString();
    }
}