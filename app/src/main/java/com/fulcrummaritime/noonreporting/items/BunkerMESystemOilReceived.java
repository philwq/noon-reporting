
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.InputType;

import com.fulcrummaritime.noonreporting.R;

import layout.EditTextWithTitleFragment;

public class BunkerMESystemOilReceived extends BaseItem {

    public final String FIELD_CODE = "RS";
    private Context context;
    EditTextWithTitleFragment rootItem;

    public BunkerMESystemOilReceived(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = EditTextWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.bunker_me_system_oil_received_label_text));
        rootItem.setInputType(InputType.TYPE_CLASS_NUMBER);
        rootItem.setWidth(66);
        rootItem.setMaxLength(4);
        shadowValue = getValue();
    }

    public void setText(String text) {
        rootItem.setText(text);
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid(boolean bunkerItemsDirty) {
        if (bunkerItemsDirty)
            return rootItem.isValid(context.getString(R.string.bunker_me_system_oil_received_error_text));
        else
            return true;
    }

    public String getValue() {
        return rootItem.getValueAsString();
    }
}