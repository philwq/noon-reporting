
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.RadioGroupWithTitleFragment;

public class HoldNumber extends BaseItem {

    private Context context;
    RadioGroupWithTitleFragment rootItem;

    public HoldNumber(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = RadioGroupWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.hold_number_label_text));
        rootItem.initialiseRadioButton(0, context.getString(R.string.hold_number_one_label_text));
        rootItem.initialiseRadioButton(1, context.getString(R.string.hold_number_two_label_text));
        rootItem.initialiseRadioButton(2, context.getString(R.string.hold_number_three_label_text));
        rootItem.initialiseRadioButton(3, context.getString(R.string.hold_number_four_label_text));
        rootItem.initialiseRadioButton(4, context.getString(R.string.hold_number_five_label_text));

        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.hold_number_error_text));
    }

    public void setEnabled(boolean enabled) {
        rootItem.setEnabled(enabled);
    }

    public String getValue() {
        return rootItem.getValue();
    }

    public void clearError() {
        rootItem.clearError();
    }
}