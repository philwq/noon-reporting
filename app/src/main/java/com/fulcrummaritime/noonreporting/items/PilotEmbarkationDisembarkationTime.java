
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.TimeTimeDatePickerWithTitleFragment;

public class PilotEmbarkationDisembarkationTime extends BaseItem {

    public final String FIELD_CODE = "PE";
    private Context context;
    TimeTimeDatePickerWithTitleFragment rootItem;

    public PilotEmbarkationDisembarkationTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = TimeTimeDatePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.pilot_embarkation_disembarkation_time_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getValue();
    }
}