
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.TimeTimeDatePickerWithTitleFragment;

public class FullAwayTime extends BaseItem {

    public final String FIELD_CODE = "FA";
    private Context context;
    TimeTimeDatePickerWithTitleFragment rootItem;

    public FullAwayTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = TimeTimeDatePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.full_away_time_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getValue();
    }
}