
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.RadioGroupWithTitleFragment;

public class CargoTypeUnits extends BaseItem {

    private Context context;
    RadioGroupWithTitleFragment rootItem;

    public CargoTypeUnits(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = RadioGroupWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.cargo_type_units_label_text));
        rootItem.initialiseRadioButton(0, context.getString(R.string.cargo_type_units_liters_label_text));
        rootItem.initialiseRadioButton(1, context.getString(R.string.cargo_type_units_kilograms_label_text));
        rootItem.initialiseRadioButton(2, context.getString(R.string.cargo_type_units_metric_tons_label_text));

        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.deck_error_text));
    }

    public String getValue() {
        if (rootItem.getValue().equalsIgnoreCase(context.getString(R.string.cargo_type_units_error_text)))
            return "W";
        else if (rootItem.getValue().equalsIgnoreCase(context.getString(R.string.eta_value_agw_label_text)))
            return "A";
        else
            return "";
    }

    public void clearError() {
        rootItem.clearError();
    }
}