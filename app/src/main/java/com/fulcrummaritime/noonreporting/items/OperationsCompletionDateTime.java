
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.DateTimePickerWithTitleFragment;

public class OperationsCompletionDateTime extends BaseItem {

    public final String FIELD_CODE = "OE";
    private Context context;
    DateTimePickerWithTitleFragment rootItem;

    public OperationsCompletionDateTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = DateTimePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.operations_completion_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getDateAsYYYYMMDD() + rootItem.getTimeAsHHMM();
    }
}