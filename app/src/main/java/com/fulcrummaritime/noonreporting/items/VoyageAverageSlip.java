
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;

import com.fulcrummaritime.noonreporting.R;

import java.util.regex.Pattern;

import layout.EditTextWithTitleFragment;

public class VoyageAverageSlip extends BaseItem {

    public final String FIELD_CODE = "SV";
    private Context context;
    EditTextWithTitleFragment rootItem;

    public VoyageAverageSlip(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = EditTextWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.voyage_average_slip_label_text));
        rootItem.setInputType(InputType.TYPE_CLASS_TEXT);
        rootItem.setWidth(78);
        rootItem.setMaxLength(5);
        rootItem.setHint(context.getString(R.string.voyage_average_slip_hint_text));

        rootItem.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // +/-DDHH
                if (s.length() > 0) {
                    if (s.charAt(0) == '+' || s.charAt(0) == '-') {
                        if (s.length() > 1) {
                            if (Character.isDigit(s.charAt(1))) {
                                if (s.length() > 2) {
                                    if (Character.isDigit(s.charAt(2))) {
                                        if (s.length() > 3) {
                                            if (s.charAt(3) == '0' || s.charAt(3) == '1' || s.charAt(3) == '2') {
                                                if (s.length() > 4) {
                                                    if (s.charAt(3) == '2') {
                                                        if (!(s.charAt(4) == '0' || s.charAt(4) == '1' || s.charAt(4) == '2' || s.charAt(4) == '3'))
                                                            s.replace(4, s.length(), s.subSequence(5, s.length()));
                                                    } else {
                                                        if (!Character.isDigit(s.charAt(4)))
                                                            s.replace(4, s.length(), s.subSequence(5, s.length()));
                                                    }
                                                }
                                            } else
                                                s.replace(3, s.length(), s.subSequence(4, s.length()));
                                        }
                                    } else
                                        s.replace(2, s.length(), s.subSequence(3, s.length()));
                                }
                            } else
                                s.replace(1, s.length(), s.subSequence(2, s.length()));
                        }
                    } else
                        s.replace(0, s.length(), s.subSequence(1, s.length()));
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        shadowValue = getValue();
    }

    public void setText(String text) {
        rootItem.setText(text);
    }


    public boolean isValid() {
        if (rootItem.isValid(context.getString(R.string.voyage_average_slip_error_text))) {
            if (Pattern.compile("[+-]\\d\\d(([0-1]\\d)|2[0-3])").matcher(rootItem.getValueAsString()).matches())
                return true;
            else {
                rootItem.setError(context.getString(R.string.voyage_average_slip_format_error_text));
                return false;
            }
        } else
            return false;
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getValueAsString();
    }
}
