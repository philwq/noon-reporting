
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.widget.EditText;

import com.fulcrummaritime.noonreporting.R;

import layout.EditTextWithTitleFragment;

public class TranshipRemarks extends BaseItem {

    public final String FIELD_CODE = "RM";
    private Context context;
    EditTextWithTitleFragment rootItem;

    public TranshipRemarks(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = EditTextWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.tranship_remarks_label_text));
        rootItem.setInputType(InputType.TYPE_CLASS_TEXT);
        // rootItem.setMultiLine(2, 2);
        rootItem.setGravity(Gravity.TOP | Gravity.LEFT);
        rootItem.setWidth(675);
        rootItem.setMaxLength(100);

        /*
        rootItem.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                for (int i=0; i<s.length(); i++)
                    if (s.charAt(i) != ' ' && !Character.isLetterOrDigit(s.charAt(i)) )
                        s.replace(i, i+1, "");
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        */

        shadowValue = getValue();
    }

    public void setText(String text) {
        rootItem.setText(text);
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.tranship_remarks_error_text));
    }

    public String getValue() {
        return rootItem.getValueAsString();
    }
}