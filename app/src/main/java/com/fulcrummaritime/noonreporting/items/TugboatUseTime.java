
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.TimePickerWithTitleFragment;

public class TugboatUseTime extends BaseItem {

    public final String FIELD_CODE = "TH";
    private Context context;
    TimePickerWithTitleFragment rootItem;

    public TugboatUseTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = TimePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.tugboat_use_hours_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getTimeAsHHMM();
    }
}