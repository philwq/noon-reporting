
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.InputType;

import com.fulcrummaritime.noonreporting.R;

import layout.EditTextWithTitleFragment;

public class TotalConsumptionDieselOilDMB extends BaseItem {

    public final String FIELD_CODE = "UB";
    private Context context;
    EditTextWithTitleFragment rootItem;

    public TotalConsumptionDieselOilDMB(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = EditTextWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.total_consumption_diesel_oil_dmb_label_text));
        rootItem.setInputType(InputType.TYPE_CLASS_NUMBER);
        rootItem.setWidth(66);
        rootItem.setMaxLength(4);
        shadowValue = getValue();
    }

    public void setText(String text) {
        rootItem.setText(text);
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.total_consumption_diesel_oil_dmb_error_text));
    }

    public String getValue() {
        return rootItem.getValueAsString();
    }
}