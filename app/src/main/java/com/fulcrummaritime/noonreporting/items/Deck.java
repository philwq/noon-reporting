
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import android.view.View;

import com.fulcrummaritime.noonreporting.R;

import layout.RadioGroupWithTitleFragment;

public class Deck extends BaseItem {

    final private Context context;
    RadioGroupWithTitleFragment rootItem;
    HoldNumber holdNumber;

    public Deck(final Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = RadioGroupWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.deck_label_text));
        rootItem.initialiseRadioButton(0, context.getString(R.string.deck_letter_a_label_text));
        rootItem.initialiseRadioButton(1, context.getString(R.string.deck_letter_b_label_text));
        rootItem.initialiseRadioButton(2, context.getString(R.string.deck_letter_c_label_text));
        rootItem.initialiseRadioButton(3, context.getString(R.string.deck_letter_d_label_text));
        rootItem.initialiseRadioButton(4, context.getString(R.string.deck_letter_w_label_text));

        for (int i=0; i<rootItem.radioButtons.length; i++) {
            rootItem.radioButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearError();
                    if (holdNumber != null)
                        holdNumber.setEnabled(!getValue().equalsIgnoreCase(context.getString(R.string.deck_letter_w_label_text)));
                }
            });
        }

        shadowValue = getValue();
    }

    public void setHoldNumber(HoldNumber holdNumber) {
        this.holdNumber = holdNumber;
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.deck_error_text));
    }

    public String getValue() {
        return rootItem.getValue();
    }

    public void clearError() {
        rootItem.clearError();
    }
}