
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.RadioGroupWithTitleFragment;

public class ETAValue extends BaseItem {

    public final String FIELD_CODE = "EV";
    private Context context;
    RadioGroupWithTitleFragment rootItem;

    public ETAValue(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = RadioGroupWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.eta_value_label_text));
        rootItem.initialiseRadioButton(0, context.getString(R.string.eta_value_wp_label_text));
        rootItem.initialiseRadioButton(1, context.getString(R.string.eta_value_agw_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid() {
        return rootItem.isValid(context.getString(R.string.eta_value_error_text));
    }

    public String getValue() {
        if (rootItem.getValue().equalsIgnoreCase(context.getString(R.string.eta_value_wp_label_text)))
            return "W";
        else if (rootItem.getValue().equalsIgnoreCase(context.getString(R.string.eta_value_agw_label_text)))
            return "A";
        else
            return "";
    }

    public void clearError() {
        rootItem.clearError();
    }
}