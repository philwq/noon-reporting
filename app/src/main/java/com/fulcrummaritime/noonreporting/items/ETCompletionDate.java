
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.DatePickerWithTitleFragment;

public class ETCompletionDate extends BaseItem {

    public final String FIELD_CODE = "EC";
    private Context context;
    DatePickerWithTitleFragment rootItem;

    public ETCompletionDate(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = DatePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.et_operations_completion_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getDateAsYYYYMMDD();
    }
}