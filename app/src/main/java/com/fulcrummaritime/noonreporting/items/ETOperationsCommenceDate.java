
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.DatePickerWithTitleFragment;

public class ETOperationsCommenceDate extends BaseItem {

    public final String FIELD_CODE = "EB";
    private Context context;
    DatePickerWithTitleFragment rootItem;

    public ETOperationsCommenceDate(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = DatePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.et_operations_commence_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getDateAsYYYYMMDD();
    }
}