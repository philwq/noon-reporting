
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.DateAMPMPickerWithTitleFragment;

public class EstimatedBerthingTime extends BaseItem {

    public final String FIELD_CODE = "EB";
    private Context context;
    DateAMPMPickerWithTitleFragment rootItem;

    public EstimatedBerthingTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = DateAMPMPickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.estimated_berthing_time_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getDateAsYYYYMMDD() + rootItem.getAMPM();
    }
}