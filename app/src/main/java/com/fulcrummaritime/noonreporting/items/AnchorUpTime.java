
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.TimePickerWithTitleFragment;

public class AnchorUpTime extends BaseItem {

    public final String FIELD_CODE = "AU";
    private Context context;
    TimePickerWithTitleFragment rootItem;

    public AnchorUpTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = TimePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.anchor_up_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getTimeAsHHMM();
    }
}