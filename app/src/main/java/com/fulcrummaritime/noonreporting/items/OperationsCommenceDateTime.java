
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.DateTimePickerWithTitleFragment;

public class OperationsCommenceDateTime extends BaseItem {

    public final String FIELD_CODE = "OB";
    private Context context;
    DateTimePickerWithTitleFragment rootItem;

    public OperationsCommenceDateTime(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = DateTimePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.operations_commence_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getDateAsYYYYMMDD() + rootItem.getTimeAsHHMM();
    }
}