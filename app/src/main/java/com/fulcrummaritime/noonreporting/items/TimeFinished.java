
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;

import com.fulcrummaritime.noonreporting.R;

import layout.TimeTimeDatePickerWithTitleFragment;

public class TimeFinished extends BaseItem {

    public final String FIELD_CODE = "TF";
    private Context context;
    TimeTimeDatePickerWithTitleFragment rootItem;

    public TimeFinished(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = TimeTimeDatePickerWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.time_finished_label_text));
        shadowValue = getValue();
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public String getValue() {
        return rootItem.getValue();
    }
}
