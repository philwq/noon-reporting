
package com.fulcrummaritime.noonreporting.items;

import android.content.Context;
import android.text.InputType;

import com.fulcrummaritime.noonreporting.R;

import layout.EditTextWithTitleFragment;

public class BunkerAESystemOilReceived extends BaseItem {

    public final String FIELD_CODE = "RA";
    private Context context;
    EditTextWithTitleFragment rootItem;

    public BunkerAESystemOilReceived(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        rootItem = EditTextWithTitleFragment.newInstance(fragment);
        rootItem.setLabelText(context.getString(R.string.bunker_ae_system_oil_received_label_text));
        rootItem.setInputType(InputType.TYPE_CLASS_NUMBER);
        rootItem.setWidth(78);
        rootItem.setMaxLength(5);
        shadowValue = getValue();
    }

    public void setText(String text) {
        rootItem.setText(text);
    }

    public boolean isDirty() {
        return super.isDirty(getValue());
    }

    public boolean isValid(boolean bunkerItemsDirty) {
        if (bunkerItemsDirty)
            return rootItem.isValid(context.getString(R.string.bunker_ae_system_oil_received_error_text));
        else
            return true;
    }

    public String getValue() {
        return rootItem.getValueAsString();
    }
}
