
package com.fulcrummaritime.noonreporting.reports;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.fulcrummaritime.noonreporting.BaseActivity;
import com.fulcrummaritime.noonreporting.R;
import com.fulcrummaritime.noonreporting.Shared;
import com.fulcrummaritime.noonreporting.items.AllFastTime;
import com.fulcrummaritime.noonreporting.items.AnchorUpTime;
import com.fulcrummaritime.noonreporting.items.ETOperationsCommenceDateTime;
import com.fulcrummaritime.noonreporting.items.ETOperationsCompletionDateTime;
import com.fulcrummaritime.noonreporting.items.EngineStandbyDateTime;
import com.fulcrummaritime.noonreporting.items.FinishWithEnginesTime;
import com.fulcrummaritime.noonreporting.items.OperationsCommenceDateTime;
import com.fulcrummaritime.noonreporting.items.OperationsCompletionDateTime;
import com.fulcrummaritime.noonreporting.items.PortName;
import com.fulcrummaritime.noonreporting.items.TugboatUseNumber;
import com.fulcrummaritime.noonreporting.items.TugboatUseTime;

import layout.TimePickerWithTitleFragment;

import layout.DateTimePickerWithTitleFragment;
import layout.EditTextWithTitleFragment;
import layout.ReportDividerFragment;
import layout.SendReportFragment;

public class BerthingReportActivity extends BaseActivity implements ReportDividerFragment.OnFragmentInteractionListener,
        TimePickerWithTitleFragment.OnFragmentInteractionListener, DateTimePickerWithTitleFragment.OnFragmentInteractionListener, SendReportFragment.OnFragmentInteractionListener,
        EditTextWithTitleFragment.OnFragmentInteractionListener {

    final Shared shared = new Shared(this);
    PortName portName;
    EngineStandbyDateTime engineStandbyDateTime;
    AnchorUpTime anchorUpTime;
    AllFastTime allFastTime;
    FinishWithEnginesTime finishWithEnginesTime;
    TugboatUseNumber tugboatUseNumber;
    TugboatUseTime tugboatUseTime;
    ETOperationsCommenceDateTime etOperationsCommenceDateTime;
    OperationsCommenceDateTime operationsCommenceDateTime;
    ETOperationsCompletionDateTime etOperationsCompletionDateTime;
    OperationsCompletionDateTime operationsCompletionDateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.berthing_activity_menu_title);
        setContentView(R.layout.activity_berthing_report);

        portName = new PortName(this, fragmentManager.findFragmentById(R.id.portNameFragment));
        engineStandbyDateTime = new EngineStandbyDateTime(this, fragmentManager.findFragmentById(R.id.engineStandByFragment));
        anchorUpTime = new AnchorUpTime(this, fragmentManager.findFragmentById(R.id.anchorUpFragment));
        allFastTime = new AllFastTime(this, fragmentManager.findFragmentById(R.id.allFastFragment));
        finishWithEnginesTime = new FinishWithEnginesTime(this, fragmentManager.findFragmentById(R.id.finishWithEnginesFragment));
        tugboatUseNumber = new TugboatUseNumber(this, fragmentManager.findFragmentById(R.id.tugboatUseNumberFragment));
        tugboatUseTime = new TugboatUseTime(this, fragmentManager.findFragmentById(R.id.tugboatUseHoursFragment));
        etOperationsCommenceDateTime = new ETOperationsCommenceDateTime(this, fragmentManager.findFragmentById(R.id.etOperationsCommenceFragment));
        operationsCommenceDateTime = new OperationsCommenceDateTime(this, fragmentManager.findFragmentById(R.id.operationsCommenceFragment));
        etOperationsCompletionDateTime = new ETOperationsCompletionDateTime(this, fragmentManager.findFragmentById(R.id.etOperationsCompletionFragment));
        operationsCompletionDateTime = new OperationsCompletionDateTime(this, fragmentManager.findFragmentById(R.id.operationsCompletionFragment));

        // Pre-populate for testing.
//        portName.setText("Port of Romford");
//        tugboatUseNumber.setText("34");
    }

    public void sendReport(View view) {
        if (portName.isValid()
            && tugboatUseNumber.isValid()) {

            shared.sendPayload(this, "INT&BER"
                + "&" + portName.FIELD_CODE + "=" + portName.getValue()
                + "&" + engineStandbyDateTime.FIELD_CODE + "=" + engineStandbyDateTime.getValue()
                + "&" + anchorUpTime.FIELD_CODE + "=" + anchorUpTime.getValue()
                + "&" + allFastTime.FIELD_CODE + "=" + allFastTime.getValue()
                + "&" + finishWithEnginesTime.FIELD_CODE + "=" + finishWithEnginesTime.getValue()
                + "&" + tugboatUseNumber.FIELD_CODE + "=" + tugboatUseNumber.getValue()
                + "&" + tugboatUseTime.FIELD_CODE + "=" + tugboatUseTime.getValue()
                + "&" + etOperationsCommenceDateTime.FIELD_CODE + "=" + etOperationsCommenceDateTime.getValue()
                + "&" + operationsCommenceDateTime.FIELD_CODE + "=" + operationsCommenceDateTime.getValue()
                + "&" + etOperationsCompletionDateTime.FIELD_CODE + "=" + etOperationsCompletionDateTime.getValue()
                + "&" + operationsCompletionDateTime.FIELD_CODE + "=" + operationsCompletionDateTime.getValue()
                + "&");
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) { }

    @Override
    public void onBackPressed() {
        if (isDirty())
            shared.activityOnBackPressed();
        else
            finish();
    }

    boolean isDirty() {
        return portName.isDirty()
            || engineStandbyDateTime.isDirty()
            || anchorUpTime.isDirty()
            || allFastTime.isDirty()
            || finishWithEnginesTime.isDirty()
            || tugboatUseNumber.isDirty()
            || tugboatUseTime.isDirty()
            || etOperationsCommenceDateTime.isDirty()
            || operationsCommenceDateTime.isDirty()
            || etOperationsCompletionDateTime.isDirty()
            || operationsCompletionDateTime.isDirty();
    }
}