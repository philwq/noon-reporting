
package com.fulcrummaritime.noonreporting.reports;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.fulcrummaritime.noonreporting.BaseActivity;
import com.fulcrummaritime.noonreporting.R;
import com.fulcrummaritime.noonreporting.Shared;
import com.fulcrummaritime.noonreporting.items.BunkerAESystemOilReceived;
import com.fulcrummaritime.noonreporting.items.BunkerDieselOilDMAReceived;
import com.fulcrummaritime.noonreporting.items.BunkerDieselOilDMBReceived;
import com.fulcrummaritime.noonreporting.items.BunkerFreshWaterReceived;
import com.fulcrummaritime.noonreporting.items.BunkerHSFuelOilRMEReceived;
import com.fulcrummaritime.noonreporting.items.BunkerLSFuelOilRMDReceived;
import com.fulcrummaritime.noonreporting.items.BunkerMECylinderOilReceived;
import com.fulcrummaritime.noonreporting.items.BunkerMESystemOilReceived;
import com.fulcrummaritime.noonreporting.items.DraftAft;
import com.fulcrummaritime.noonreporting.items.DraftForward;
import com.fulcrummaritime.noonreporting.items.FullAwayTime;
import com.fulcrummaritime.noonreporting.items.NextPortEstimatedArrival;
import com.fulcrummaritime.noonreporting.items.NextPortEstimatedDraftAft;
import com.fulcrummaritime.noonreporting.items.NextPortEstimatedDraftForward;
import com.fulcrummaritime.noonreporting.items.NextPortName;
import com.fulcrummaritime.noonreporting.items.PilotEmbarkationDisembarkationTime;
import com.fulcrummaritime.noonreporting.items.PortName;
import com.fulcrummaritime.noonreporting.items.TimeFinished;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerAESystemOil;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerDieselOilDMA;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerDieselOilDMB;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerFreshWater;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerHSFuelOilRME;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerLSFuelOilRMD;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerMECylinderOil;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerMESystemOil;
import com.fulcrummaritime.noonreporting.items.Trim;

import layout.DateAMPMPickerWithTitleFragment;
import layout.DateTimePickerWithTitleFragment;
import layout.EditTextWithTitleFragment;
import layout.ReportDividerFragment;
import layout.SendReportFragment;
import layout.TimePickerWithTitleFragment;
import layout.TimeTimeDatePickerWithTitleFragment;

public class DepartureReportActivity extends BaseActivity implements ReportDividerFragment.OnFragmentInteractionListener,
        TimePickerWithTitleFragment.OnFragmentInteractionListener, DateTimePickerWithTitleFragment.OnFragmentInteractionListener,
        SendReportFragment.OnFragmentInteractionListener, EditTextWithTitleFragment.OnFragmentInteractionListener,
        DateAMPMPickerWithTitleFragment.OnFragmentInteractionListener, TimeTimeDatePickerWithTitleFragment.OnFragmentInteractionListener {

    final Shared shared = new Shared(this);
    PortName portName;
    DraftForward draftForward;
    DraftAft draftAft;
    Trim trim;
    TotalROBBunkerLSFuelOilRMD totalROBBunkerLSFuelOilRMD;
    TotalROBBunkerHSFuelOilRME totalROBBunkerHSFuelOilRME;
    TotalROBBunkerDieselOilDMA totalROBBunkerDieselOilDMA;
    TotalROBBunkerDieselOilDMB totalROBBunkerDieselOilDMB;
    TotalROBBunkerMESystemOil totalROBBunkerMESystemOil;
    TotalROBBunkerMECylinderOil totalROBBunkerMECylinderOil;
    TotalROBBunkerAESystemOil totalROBBunkerAESystemOil;
    TotalROBBunkerFreshWater totalROBBunkerFreshWater;
    public BunkerLSFuelOilRMDReceived bunkerLSFuelOilRMDReceived;
    public BunkerHSFuelOilRMEReceived bunkerHSFuelOilRMEReceived;
    public BunkerDieselOilDMAReceived bunkerDieselOilDMAReceived;
    public BunkerDieselOilDMBReceived bunkerDieselOilDMBReceived;
    public BunkerMESystemOilReceived bunkerMESystemOilReceived;
    public BunkerMECylinderOilReceived bunkerMECylinderOilReceived;
    public BunkerAESystemOilReceived bunkerAESystemOilReceived;
    public BunkerFreshWaterReceived bunkerFreshWaterReceived;
    TimeFinished timeFinished;
    PilotEmbarkationDisembarkationTime PilotEmbarkationDisembarkationTime;
    FullAwayTime fullAwayTime;
    NextPortName nextPortName;
    NextPortEstimatedArrival nextPortEstimatedArrival;
    NextPortEstimatedDraftForward nextPortEstimatedDraftForward;
    NextPortEstimatedDraftAft nextPortEstimatedDraftAft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.departure_activity_menu_title);
        setContentView(R.layout.activity_departure_report);

        portName = new PortName(this, fragmentManager.findFragmentById(R.id.portNameFragment));
        draftForward = new DraftForward(this, fragmentManager.findFragmentById(R.id.draftForwardFragment));
        draftAft = new DraftAft(this, fragmentManager.findFragmentById(R.id.draftAftFragment));
        trim = new Trim(this, fragmentManager.findFragmentById(R.id.trimFragment));
        totalROBBunkerLSFuelOilRMD = new TotalROBBunkerLSFuelOilRMD(this, fragmentManager.findFragmentById(R.id.totalROBBunkerLSFuelOilRMDFragment));
        totalROBBunkerHSFuelOilRME = new TotalROBBunkerHSFuelOilRME(this, fragmentManager.findFragmentById(R.id.totalROBBunkerHSFuelOilRMEFragment));
        totalROBBunkerDieselOilDMA = new TotalROBBunkerDieselOilDMA(this, fragmentManager.findFragmentById(R.id.totalROBBunkerDieselOilDMAFragment));
        totalROBBunkerDieselOilDMB = new TotalROBBunkerDieselOilDMB(this, fragmentManager.findFragmentById(R.id.totalROBBunkerDieselOilDMBFragment));
        totalROBBunkerMESystemOil = new TotalROBBunkerMESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerMESystemOilFragment));
        totalROBBunkerMECylinderOil = new TotalROBBunkerMECylinderOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerMECylinderOilFragment));
        totalROBBunkerAESystemOil = new TotalROBBunkerAESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerAESystemOilFragment));
        totalROBBunkerFreshWater = new TotalROBBunkerFreshWater(this, fragmentManager.findFragmentById(R.id.totalROBBunkerFreshWaterFragment));
        bunkerLSFuelOilRMDReceived = new BunkerLSFuelOilRMDReceived(this, fragmentManager.findFragmentById(R.id.bunkerLSFuelOilRMDReceivedFragment));
        bunkerHSFuelOilRMEReceived = new BunkerHSFuelOilRMEReceived(this, fragmentManager.findFragmentById(R.id.bunkerHSFuelOilRMEReceivedFragment));
        bunkerDieselOilDMAReceived = new BunkerDieselOilDMAReceived(this, fragmentManager.findFragmentById(R.id.bunkerDieselOilDMAReceivedFragment));
        bunkerDieselOilDMBReceived = new BunkerDieselOilDMBReceived(this, fragmentManager.findFragmentById(R.id.bunkerDieselOilDMBReceivedFragment));
        bunkerMESystemOilReceived = new BunkerMESystemOilReceived(this, fragmentManager.findFragmentById(R.id.bunkerMESystemOilReceivedFragment));
        bunkerMECylinderOilReceived = new BunkerMECylinderOilReceived(this, fragmentManager.findFragmentById(R.id.bunkerMECylinderOilReceivedFragment));
        bunkerAESystemOilReceived = new BunkerAESystemOilReceived(this, fragmentManager.findFragmentById(R.id.bunkerAESystemOilReceivedFragment));
        bunkerFreshWaterReceived = new BunkerFreshWaterReceived(this, fragmentManager.findFragmentById(R.id.bunkerFreshWaterReceivedFragment));
        timeFinished = new TimeFinished(this, fragmentManager.findFragmentById(R.id.timeFinishedFragment));
        PilotEmbarkationDisembarkationTime = new PilotEmbarkationDisembarkationTime(this, fragmentManager.findFragmentById(R.id.pilotEmbarkationDisembarkationTimeFragment));
        fullAwayTime = new FullAwayTime(this, fragmentManager.findFragmentById(R.id.fullAwayTimeFragment));
        nextPortName = new NextPortName(this, fragmentManager.findFragmentById(R.id.nextPortNameFragment));
        nextPortEstimatedArrival = new NextPortEstimatedArrival(this, fragmentManager.findFragmentById(R.id.nextPortEstimatedArrivalFragment));
        nextPortEstimatedDraftForward = new NextPortEstimatedDraftForward(this, fragmentManager.findFragmentById(R.id.nextPortEstimatedDraftForwardFragment));
        nextPortEstimatedDraftAft = new NextPortEstimatedDraftAft(this, fragmentManager.findFragmentById(R.id.nextPortEstimatedDraftAftFragment));

        // Pre-populate for testing.
//        portName.setText("Port of Romford");
//        draftForward.setText("89");
//        draftAft.setText("90");
//        trim.setText("95");
//        totalROBBunkerLSFuelOilRMD.setText("9070");
//        totalROBBunkerHSFuelOilRME.setText("9080");
//        totalROBBunkerDieselOilDMA.setText("9090");
//        totalROBBunkerDieselOilDMB.setText("9100");
//        totalROBBunkerMESystemOil.setText("9110");
//        totalROBBunkerMECylinderOil.setText("91101");
//        totalROBBunkerAESystemOil.setText("91102");
//        totalROBBunkerFreshWater.setText("9200");
//        bunkerLSFuelOilRMDReceived.setText("9200");
//        bunkerHSFuelOilRMEReceived.setText("9210");
//        bunkerDieselOilDMAReceived.setText("9220");
//        bunkerDieselOilDMBReceived.setText("9230");
//        bunkerMESystemOilReceived.setText("9310");
//        bunkerMECylinderOilReceived.setText("9320");
//        bunkerAESystemOilReceived.setText("9330");
//        bunkerFreshWaterReceived.setText("9340");
//        nextPortName.setText("Port Hornchurch");
    }

    public void sendReport(View view) {

        boolean bunkerItemsDirty = bunkerLSFuelOilRMDReceived.isDirty()
            || bunkerHSFuelOilRMEReceived.isDirty()
            || bunkerDieselOilDMAReceived.isDirty()
            || bunkerDieselOilDMBReceived.isDirty()
            || bunkerMESystemOilReceived.isDirty()
            || bunkerMECylinderOilReceived.isDirty()
            || bunkerAESystemOilReceived.isDirty()
            || bunkerFreshWaterReceived.isDirty();

        if (portName.isValid()
            && draftForward.isValid()
            && draftAft.isValid()
            && trim.isValid()
            && totalROBBunkerLSFuelOilRMD.isValid()
            && totalROBBunkerHSFuelOilRME.isValid()
            && totalROBBunkerDieselOilDMA.isValid()
            && totalROBBunkerDieselOilDMB.isValid()
            && totalROBBunkerMESystemOil.isValid()
            && totalROBBunkerMECylinderOil.isValid()
            && totalROBBunkerAESystemOil.isValid()
            && totalROBBunkerFreshWater.isValid()
            && bunkerLSFuelOilRMDReceived.isValid(bunkerItemsDirty)
            && bunkerHSFuelOilRMEReceived.isValid(bunkerItemsDirty)
            && bunkerDieselOilDMAReceived.isValid(bunkerItemsDirty)
            && bunkerDieselOilDMBReceived.isValid(bunkerItemsDirty)
            && bunkerMESystemOilReceived.isValid(bunkerItemsDirty)
            && bunkerMECylinderOilReceived.isValid(bunkerItemsDirty)
            && bunkerAESystemOilReceived.isValid(bunkerItemsDirty)
            && bunkerFreshWaterReceived.isValid(bunkerItemsDirty)
            && nextPortName.isValid()
            && nextPortEstimatedDraftForward.isValid()
            && nextPortEstimatedDraftAft.isValid()) {

            shared.sendPayload(this, "INT&DPA"
                    + "&" + portName.FIELD_CODE + "=" + portName.getValue()
                    + "&" + draftForward.FIELD_CODE + "=" + draftForward.getValue()
                    + "&" + draftAft.FIELD_CODE + "=" + draftAft.getValue()
                    + "&" + trim.FIELD_CODE + "=" + trim.getValue()
                    + "&" + totalROBBunkerLSFuelOilRMD.FIELD_CODE + "=" + totalROBBunkerLSFuelOilRMD.getValue()
                    + "&" + totalROBBunkerHSFuelOilRME.FIELD_CODE + "=" + totalROBBunkerHSFuelOilRME.getValue()
                    + "&" + totalROBBunkerDieselOilDMA.FIELD_CODE + "=" + totalROBBunkerDieselOilDMA.getValue()
                    + "&" + totalROBBunkerDieselOilDMB.FIELD_CODE + "=" + totalROBBunkerDieselOilDMB.getValue()
                    + "&" + totalROBBunkerMESystemOil.FIELD_CODE + "=" + totalROBBunkerMESystemOil.getValue()
                    + "&" + totalROBBunkerMECylinderOil.FIELD_CODE + "=" + totalROBBunkerMECylinderOil.getValue()
                    + "&" + totalROBBunkerAESystemOil.FIELD_CODE + "=" + totalROBBunkerAESystemOil.getValue()
                    + "&" + totalROBBunkerFreshWater.FIELD_CODE + "=" + totalROBBunkerFreshWater.getValue()
                    + "&" + bunkerLSFuelOilRMDReceived.FIELD_CODE + "=" + bunkerLSFuelOilRMDReceived.getValue()
                    + "&" + bunkerHSFuelOilRMEReceived.FIELD_CODE + "=" + bunkerHSFuelOilRMEReceived.getValue()
                    + "&" + bunkerDieselOilDMAReceived.FIELD_CODE + "=" + bunkerDieselOilDMAReceived.getValue()
                    + "&" + bunkerDieselOilDMBReceived.FIELD_CODE + "=" + bunkerDieselOilDMBReceived.getValue()
                    + "&" + bunkerMESystemOilReceived.FIELD_CODE + "=" + bunkerMESystemOilReceived.getValue()
                    + "&" + bunkerMECylinderOilReceived.FIELD_CODE + "=" + bunkerMECylinderOilReceived.getValue()
                    + "&" + bunkerAESystemOilReceived.FIELD_CODE + "=" + bunkerAESystemOilReceived.getValue()
                    + "&" + bunkerFreshWaterReceived.FIELD_CODE + "=" + bunkerFreshWaterReceived.getValue()
                    + "&" + timeFinished.FIELD_CODE + "=" + timeFinished.getValue()
                    + "&" + PilotEmbarkationDisembarkationTime.FIELD_CODE + "=" + PilotEmbarkationDisembarkationTime.getValue()
                    + "&" + fullAwayTime.FIELD_CODE + "=" + fullAwayTime.getValue()
                    + "&" + nextPortName.FIELD_CODE + "=" + nextPortName.getValue()
                    + "&" + nextPortEstimatedArrival.FIELD_CODE + "=" + nextPortEstimatedArrival.getValue()
                    + "&" + nextPortEstimatedDraftForward.FIELD_CODE + "=" + nextPortEstimatedDraftForward.getValue()
                    + "&" + nextPortEstimatedDraftAft.FIELD_CODE + "=" + nextPortEstimatedDraftAft.getValue()
                    + "&");
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) { }

    @Override
    public void onBackPressed() {
        if (isDirty())
            shared.activityOnBackPressed();
        else
            finish();
    }

    boolean isDirty() {
        return portName.isDirty()
            || draftForward.isDirty()
            || draftAft.isDirty()
            || trim.isDirty()
            || totalROBBunkerLSFuelOilRMD.isDirty()
            || totalROBBunkerHSFuelOilRME.isDirty()
            || totalROBBunkerDieselOilDMA.isDirty()
            || totalROBBunkerDieselOilDMB.isDirty()
            || totalROBBunkerMESystemOil.isDirty()
            || totalROBBunkerMECylinderOil.isDirty()
            || totalROBBunkerAESystemOil.isDirty()
            || totalROBBunkerFreshWater.isDirty()
            || bunkerLSFuelOilRMDReceived.isDirty()
            || bunkerHSFuelOilRMEReceived.isDirty()
            || bunkerDieselOilDMAReceived.isDirty()
            || bunkerDieselOilDMBReceived.isDirty()
            || bunkerMESystemOilReceived.isDirty()
            || bunkerMECylinderOilReceived.isDirty()
            || bunkerAESystemOilReceived.isDirty()
            || bunkerFreshWaterReceived.isDirty()
            || timeFinished.isDirty()
            || PilotEmbarkationDisembarkationTime.isDirty()
            || fullAwayTime.isDirty()
            || nextPortName.isDirty()
            || nextPortEstimatedArrival.isDirty()
            || nextPortEstimatedDraftForward.isDirty()
            || nextPortEstimatedDraftAft.isDirty();
    }
}
