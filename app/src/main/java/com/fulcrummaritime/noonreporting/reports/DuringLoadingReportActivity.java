
package com.fulcrummaritime.noonreporting.reports;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.fulcrummaritime.noonreporting.BaseActivity;
import com.fulcrummaritime.noonreporting.R;
import com.fulcrummaritime.noonreporting.Shared;
import com.fulcrummaritime.noonreporting.items.CargoType;
import com.fulcrummaritime.noonreporting.items.CargoTypeUnits;
import com.fulcrummaritime.noonreporting.items.CargoTypeUnitsAllowed;
import com.fulcrummaritime.noonreporting.items.CargoTypeUnitsOnBoard;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionAESystemOil;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionDieselOilDMA;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionDieselOilDMB;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionFreshWater;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionHSFuelOil;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionLSFuelOil;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionMECylinderOil;
import com.fulcrummaritime.noonreporting.items.DailyConsumptionMESystemOil;
import com.fulcrummaritime.noonreporting.items.Deck;
import com.fulcrummaritime.noonreporting.items.ETAValue;
import com.fulcrummaritime.noonreporting.items.ETCompletionDate;
import com.fulcrummaritime.noonreporting.items.ForwardToVesselName;
import com.fulcrummaritime.noonreporting.items.HoldNumber;
import com.fulcrummaritime.noonreporting.items.PortName;
import com.fulcrummaritime.noonreporting.items.TotalROBAESystemOil;
import com.fulcrummaritime.noonreporting.items.TotalROBDieselOilDMA;
import com.fulcrummaritime.noonreporting.items.TotalROBDieselOilDMB;
import com.fulcrummaritime.noonreporting.items.TotalROBFreshWater;
import com.fulcrummaritime.noonreporting.items.TotalROBHSFuelOilRME;
import com.fulcrummaritime.noonreporting.items.TotalROBLSFuelOilRMD;
import com.fulcrummaritime.noonreporting.items.TotalROBMECylinderOil;
import com.fulcrummaritime.noonreporting.items.TotalROBMESystemOil;
import com.fulcrummaritime.noonreporting.items.TranshipFromVesselName;
import com.fulcrummaritime.noonreporting.items.TranshipRemarks;

import layout.DatePickerWithTitleFragment;
import layout.DateTimePickerWithTitleFragment;
import layout.EditTextWithTitleFragment;
import layout.RadioGroupWithTitleFragment;
import layout.ReportDividerFragment;
import layout.SendReportFragment;

public class DuringLoadingReportActivity extends BaseActivity implements ReportDividerFragment.OnFragmentInteractionListener,
        SendReportFragment.OnFragmentInteractionListener, EditTextWithTitleFragment.OnFragmentInteractionListener, DateTimePickerWithTitleFragment.OnFragmentInteractionListener,
        DatePickerWithTitleFragment.OnFragmentInteractionListener, RadioGroupWithTitleFragment.OnFragmentInteractionListener {

    final Shared shared = new Shared(this);
    PortName portName;
    Deck deck;
    HoldNumber holdNumber;
    CargoType cargoType;
    // CargoTypeUnits cargoTypeUnits;
    // CargoTypeUnitsAllowed cargoTypeUnitsAllowed;
    CargoTypeUnitsOnBoard cargoTypeUnitsOnBoard;
    TranshipFromVesselName transhipFromVesselName;
    ForwardToVesselName forwardToVesselName;
    TranshipRemarks transhipRemarks;
    DailyConsumptionLSFuelOil dailyConsumptionLSFuelOil;            // Daily Consumption LS Fuel Oil (RMD-80)	UF	M	Num*4		Metric Tons - Daily Consumption LS Fuel Oil (RMD-80)
    DailyConsumptionHSFuelOil dailyConsumptionHSFuelOil;            // Daily Consumption HS Fuel Oil (RME/G-180)	UH	M	Num*4		Metric Tons - Daily Consumption HS Fuel Oil (RME/G-180)
    DailyConsumptionDieselOilDMA dailyConsumptionDieselOilDMA;      // Daily Consumption Diesel Oil (DMA)	UD	M	Num*4		Metric Tons - Daily Consumption Diesel Oil (DMA)
    DailyConsumptionDieselOilDMB dailyConsumptionDieselOilDMB;      // Daily Consumption Diesel Oil (DMB)	UB	M	Num*4		Metric Tons - Daily Consumption Diesel Oil (DMB)
    DailyConsumptionMESystemOil dailyConsumptionMESystemOil;        // Daily Consumption ME System Oil	US	M	Num*5		Litres - Daily Consumption ME System Oil
    DailyConsumptionMECylinderOil dailyConsumptionMECylinderOil;    // Daily Consumption ME Cylinder Oil	UC	M	Num*5		Litres - Daily Consumption ME Cylinder Oil
    DailyConsumptionAESystemOil dailyConsumptionAESystemOil;        // Daily Consumption AE System Oil	UA	M	Num*5		Litres - Daily Consumption AE System Oil
    DailyConsumptionFreshWater dailyConsumptionFreshWater;          // Daily Consumption Fresh Water	UW	M	Num*4		Metric Tons - Daily Consumption Fresh Water
    TotalROBLSFuelOilRMD totalROBLSFuelOilRMD;                      // Total ROB LS Fuel Oil (RMD-80)	FO	M	Num*4		Metric Tons - Total ROB LS Fuel Oil (RMD-80)
    TotalROBHSFuelOilRME totalROBHSFuelOilRME;                      // Total ROB HS Fuel Oil (RME/G-180)	HO	M	Num*4		Metric Tons - Total ROB HS Fuel Oil (RME/G-180)
    TotalROBDieselOilDMA totalROBDieselOilDMA;                      // Total ROB HS Fuel Oil (RME/G-180)	HO	M	Num*4		Metric Tons - Total ROB HS Fuel Oil (RME/G-180)
    TotalROBDieselOilDMB totalROBDieselOilDMB;                      // Total ROB Diesel Oil (DMA)	DO	M	Num*4		Metric Tons - Total ROB Diesel Oil (DMA)
    TotalROBMESystemOil totalROBMESystemOil;                        // Total ROB ME System Oil	SO	M	Num*5		Litres - Total ROB NE System Oil
    TotalROBMECylinderOil totalROBMECylinderOil;                    // Total ROB ME Cylinder Oil	CO	M	Num*5		Litres - Total ROB ME Cylinder Oil
    TotalROBAESystemOil totalROBAESystemOil;                        // Total ROB ME System Oil	AO	M	Num*5		Litres - Total ROB AE System Oil
    TotalROBFreshWater totalROBFreshWater;                          // Total ROB Fresh Water	FW	M	Num*4		Metric Tons - Total ROB Fresh Water
    ETCompletionDate etCompletionDate;                              // ET Completion	EC	M	Num*8	YYYYMMDD	Date
    ETAValue etaValue;                                              // ETA Value	EV	M	Char*1	W or A	W=WP (Weather Permitting) / A=AGW (All Going Well)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.during_loading_activity_menu_title);
        setContentView(R.layout.activity_during_loading_report);

        portName = new PortName(this, fragmentManager.findFragmentById(R.id.portNameFragment));
        deck = new Deck(this, fragmentManager.findFragmentById(R.id.deckFragment));

        holdNumber = new HoldNumber(this, fragmentManager.findFragmentById(R.id.holdNumberFragment));
        deck.setHoldNumber(holdNumber);

        cargoType = new CargoType(this, fragmentManager.findFragmentById(R.id.cargoTypeFragment));
        // cargoTypeUnits = new CargoTypeUnits(this, fragmentManager.findFragmentById(R.id.cargoTypeUnitsFragment));
        // cargoTypeUnitsAllowed = new CargoTypeUnitsAllowed(this, fragmentManager.findFragmentById(R.id.cargoTypeUnitsAllowedFragment));
        cargoTypeUnitsOnBoard = new CargoTypeUnitsOnBoard(this, fragmentManager.findFragmentById(R.id.cargoTypeUnitsOnBoardFragment));
        transhipFromVesselName = new TranshipFromVesselName(this, fragmentManager.findFragmentById(R.id.transhipFromVesselNameFragment));
        forwardToVesselName = new ForwardToVesselName(this, fragmentManager.findFragmentById(R.id.forwardToVesselNameFragment));
        transhipRemarks = new TranshipRemarks(this, fragmentManager.findFragmentById(R.id.trashipRemarksFragment));
        dailyConsumptionLSFuelOil = new DailyConsumptionLSFuelOil(this, fragmentManager.findFragmentById(R.id.dailyConsumptionLSFuelOilFragment));
        dailyConsumptionHSFuelOil = new DailyConsumptionHSFuelOil(this, fragmentManager.findFragmentById(R.id.dailyConsumptionHSFuelOilFragment));
        dailyConsumptionDieselOilDMA = new DailyConsumptionDieselOilDMA(this, fragmentManager.findFragmentById(R.id.dailyConsumptionDieselOilDMAFragment));
        dailyConsumptionDieselOilDMB = new DailyConsumptionDieselOilDMB(this, fragmentManager.findFragmentById(R.id.dailyConsumptionDieselOilDMBFragment));
        dailyConsumptionMESystemOil = new DailyConsumptionMESystemOil(this, fragmentManager.findFragmentById(R.id.dailyConsumptionMESystemOilFragment));
        dailyConsumptionMECylinderOil = new DailyConsumptionMECylinderOil(this, fragmentManager.findFragmentById(R.id.dailyConsumptionMECylinderOilFragment));
        dailyConsumptionAESystemOil = new DailyConsumptionAESystemOil(this, fragmentManager.findFragmentById(R.id.dailyConsumptionAESystemOilFragment));
        dailyConsumptionFreshWater = new DailyConsumptionFreshWater(this, fragmentManager.findFragmentById(R.id.dailyConsumptionFreshWaterFragment));
        totalROBLSFuelOilRMD = new TotalROBLSFuelOilRMD(this, fragmentManager.findFragmentById(R.id.totalROBLSFuelOilRMDFragment));
        totalROBHSFuelOilRME = new TotalROBHSFuelOilRME(this, fragmentManager.findFragmentById(R.id.totalROBHSFuelOilRMEFragment));
        totalROBDieselOilDMA = new TotalROBDieselOilDMA(this, fragmentManager.findFragmentById(R.id.totalROBDieselOilDMAFragment));
        totalROBDieselOilDMB = new TotalROBDieselOilDMB(this, fragmentManager.findFragmentById(R.id.totalROBDieselOilDMBFragment));
        totalROBMESystemOil = new TotalROBMESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBMESystemOilFragment));
        totalROBMECylinderOil = new TotalROBMECylinderOil(this, fragmentManager.findFragmentById(R.id.totalROBMECylinderOilFragment));
        totalROBAESystemOil = new TotalROBAESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBAESystemOilFragment));
        totalROBFreshWater = new TotalROBFreshWater(this, fragmentManager.findFragmentById(R.id.totalROBFreshWaterFragment));
        etCompletionDate = new ETCompletionDate(this, fragmentManager.findFragmentById(R.id.etCompletionDateFragment));
        etaValue = new ETAValue(this, fragmentManager.findFragmentById(R.id.etaValueFragment));

        // Pre-populate for testing.
//        portName.setText("Romford");
//        transhipFromVesselName.setText("FMS One");
//        forwardToVesselName.setText("FMS Two");
//        transhipRemarks.setText("Now is the time.");
//
//        dailyConsumptionLSFuelOil.setText("1010");
//        dailyConsumptionHSFuelOil.setText("1020");
//        dailyConsumptionDieselOilDMA.setText("1030");
//        dailyConsumptionDieselOilDMB.setText("1040");
//
//        dailyConsumptionMESystemOil.setText("2010");
//        dailyConsumptionMECylinderOil.setText("2020");
//        dailyConsumptionAESystemOil.setText("2030");
//        dailyConsumptionFreshWater.setText("2040");
//
//        totalROBLSFuelOilRMD.setText("3010");
//        totalROBHSFuelOilRME.setText("3020");
//        totalROBDieselOilDMA.setText("3030");
//        totalROBDieselOilDMB.setText("3040");
//
//        totalROBMESystemOil.setText("4010");
//        totalROBMECylinderOil.setText("4020");
//        totalROBAESystemOil.setText("4030");
//        totalROBFreshWater.setText("4040");
    }

    public void sendReport(View view) {
        if (portName.isValid()
            && transhipFromVesselName.isValid()
            && forwardToVesselName.isValid()
            && dailyConsumptionLSFuelOil.isValid()
            && dailyConsumptionHSFuelOil.isValid()
            && dailyConsumptionDieselOilDMA.isValid()
            && dailyConsumptionDieselOilDMB.isValid()
            && dailyConsumptionMESystemOil.isValid()
            && dailyConsumptionMECylinderOil.isValid()
            && dailyConsumptionAESystemOil.isValid()
            && dailyConsumptionFreshWater.isValid()
            && totalROBLSFuelOilRMD.isValid()
            && totalROBHSFuelOilRME.isValid()
            && totalROBDieselOilDMA.isValid()
            && totalROBDieselOilDMB.isValid()
            && totalROBMESystemOil.isValid()
            && totalROBMECylinderOil.isValid()
            && totalROBAESystemOil.isValid()
            && totalROBFreshWater.isValid()
            && etaValue.isValid()) {

            shared.sendPayload(this, "INT&DLR"
                + "&" + portName.FIELD_CODE + "=" + portName.getValue()
                + "&" + transhipFromVesselName.FIELD_CODE + "=" + transhipFromVesselName.getValue()
                + "&" + forwardToVesselName.FIELD_CODE + "=" + forwardToVesselName.getValue()
                + "&" + transhipRemarks.FIELD_CODE + "=" + transhipRemarks.getValue()
                + "&" + dailyConsumptionLSFuelOil.FIELD_CODE + "=" + dailyConsumptionLSFuelOil.getValue()
                + "&" + dailyConsumptionHSFuelOil.FIELD_CODE + "=" + dailyConsumptionHSFuelOil.getValue()
                + "&" + dailyConsumptionDieselOilDMA.FIELD_CODE + "=" + dailyConsumptionDieselOilDMA.getValue()
                + "&" + dailyConsumptionDieselOilDMB.FIELD_CODE + "=" + dailyConsumptionDieselOilDMB.getValue()
                + "&" + dailyConsumptionMESystemOil.FIELD_CODE + "=" + dailyConsumptionMESystemOil.getValue()
                + "&" + dailyConsumptionMECylinderOil.FIELD_CODE + "=" + dailyConsumptionMECylinderOil.getValue()
                + "&" + dailyConsumptionAESystemOil.FIELD_CODE + "=" + dailyConsumptionAESystemOil.getValue()
                + "&" + dailyConsumptionFreshWater.FIELD_CODE + "=" + dailyConsumptionFreshWater.getValue()
                + "&" + totalROBMESystemOil.FIELD_CODE + "=" + totalROBMESystemOil.getValue()
                + "&" + totalROBMECylinderOil.FIELD_CODE + "=" + totalROBMECylinderOil.getValue()
                + "&" + totalROBAESystemOil.FIELD_CODE + "=" + totalROBAESystemOil.getValue()
                + "&" + totalROBFreshWater.FIELD_CODE + "=" + totalROBFreshWater.getValue()
                + "&" + etCompletionDate.FIELD_CODE + "=" + etCompletionDate.getValue()
                + "&" + etaValue.FIELD_CODE + "=" + etaValue.getValue()
                + "&");
         }
    }

    @Override
    public void onFragmentInteraction(Uri uri) { }

    @Override
    public void onBackPressed() {
        if (isDirty())
            shared.activityOnBackPressed();
        else
            finish();
    }

    boolean isDirty() {
        return portName.isDirty()
            || transhipFromVesselName.isDirty()
            || forwardToVesselName.isDirty()
            || transhipRemarks.isDirty()
            || dailyConsumptionLSFuelOil.isDirty()
            || dailyConsumptionHSFuelOil.isDirty()
            || dailyConsumptionDieselOilDMA.isDirty()
            || dailyConsumptionDieselOilDMB.isDirty()
            || dailyConsumptionMESystemOil.isDirty()
            || dailyConsumptionMECylinderOil.isDirty()
            || dailyConsumptionAESystemOil.isDirty()
            || dailyConsumptionFreshWater.isDirty()
            || totalROBLSFuelOilRMD.isDirty()
            || totalROBHSFuelOilRME.isDirty()
            || totalROBDieselOilDMA.isDirty()
            || totalROBDieselOilDMB.isDirty()
            || totalROBMESystemOil.isDirty()
            || totalROBMECylinderOil.isDirty()
            || totalROBAESystemOil.isDirty()
            || totalROBFreshWater.isDirty()
            || etCompletionDate.isDirty()
            || etaValue.isDirty();
    }
}