
package com.fulcrummaritime.noonreporting.reports;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.fulcrummaritime.noonreporting.BaseActivity;
import com.fulcrummaritime.noonreporting.R;
import com.fulcrummaritime.noonreporting.Shared;
import com.fulcrummaritime.noonreporting.items.DraftAft;
import com.fulcrummaritime.noonreporting.items.DraftForward;
import com.fulcrummaritime.noonreporting.items.ETOperationsCommenceDate;
import com.fulcrummaritime.noonreporting.items.ETOperationsCommenceDateTime;
import com.fulcrummaritime.noonreporting.items.PortName;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerAESystemOil;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerBallastWater;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerDieselOilDMA;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerDieselOilDMB;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerFreshWater;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerHSFuelOilRME;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerLSFuelOilRMD;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerMECylinderOil;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerMESystemOil;

import layout.DatePickerWithTitleFragment;
import layout.DateTimePickerWithTitleFragment;
import layout.EditTextWithTitleFragment;
import layout.ReportDividerFragment;
import layout.SendReportFragment;

public class WhileAtPortReportActivity extends BaseActivity implements ReportDividerFragment.OnFragmentInteractionListener,
        SendReportFragment.OnFragmentInteractionListener, EditTextWithTitleFragment.OnFragmentInteractionListener, DateTimePickerWithTitleFragment.OnFragmentInteractionListener,
        DatePickerWithTitleFragment.OnFragmentInteractionListener {

    final Shared shared = new Shared(this);
    PortName portName;
    ETOperationsCommenceDate etOperationsCommenceDate;
    TotalROBBunkerLSFuelOilRMD totalROBBunkerLSFuelOilRMD;
    TotalROBBunkerHSFuelOilRME totalROBBunkerHSFuelOilRME;
    TotalROBBunkerDieselOilDMA totalROBBunkerDieselOilDMA;
    TotalROBBunkerDieselOilDMB totalROBBunkerDieselOilDMB;
    TotalROBBunkerMESystemOil totalROBBunkerMESystemOil;
    TotalROBBunkerMECylinderOil totalROBBunkerMECylinderOil;
    TotalROBBunkerAESystemOil totalROBBunkerAESystemOil;
    TotalROBBunkerFreshWater totalROBBunkerFreshWater;
    TotalROBBunkerBallastWater totalROBBunkerBallastWater;
    DraftForward draftForward;
    DraftAft draftAft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.while_at_port_activity_menu_title);
        setContentView(R.layout.activity_while_at_port_report);

        portName = new PortName(this, fragmentManager.findFragmentById(R.id.portNameFragment));
        etOperationsCommenceDate = new ETOperationsCommenceDate(this, fragmentManager.findFragmentById(R.id.etOperationsCommenceDateFragment));
        totalROBBunkerLSFuelOilRMD = new TotalROBBunkerLSFuelOilRMD(this, fragmentManager.findFragmentById(R.id.totalROBBunkerLSFuelOilRMDFragment));
        totalROBBunkerHSFuelOilRME = new TotalROBBunkerHSFuelOilRME(this, fragmentManager.findFragmentById(R.id.totalROBBunkerHSFuelOilRMEFragment));
        totalROBBunkerDieselOilDMA = new TotalROBBunkerDieselOilDMA(this, fragmentManager.findFragmentById(R.id.totalROBBunkerDieselOilDMAFragment));
        totalROBBunkerDieselOilDMB = new TotalROBBunkerDieselOilDMB(this, fragmentManager.findFragmentById(R.id.totalROBBunkerDieselOilDMBFragment));
        totalROBBunkerMESystemOil = new TotalROBBunkerMESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerMESystemOilFragment));
        totalROBBunkerMECylinderOil = new TotalROBBunkerMECylinderOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerMECylinderOilFragment));
        totalROBBunkerAESystemOil = new TotalROBBunkerAESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerAESystemOilFragment));
        totalROBBunkerFreshWater = new TotalROBBunkerFreshWater(this, fragmentManager.findFragmentById(R.id.totalROBBunkerFreshWaterFragment));
        totalROBBunkerBallastWater = new TotalROBBunkerBallastWater(this, fragmentManager.findFragmentById(R.id.totalROBBunkerBallastWaterFragment));
        draftForward = new DraftForward(this, fragmentManager.findFragmentById(R.id.draftForwardFragment));
        draftAft = new DraftAft(this, fragmentManager.findFragmentById(R.id.draftAftFragment));

        // Pre-populate for testing.
//        portName.setText("Port of Romford");
//        totalROBBunkerLSFuelOilRMD.setText("9070");
//        totalROBBunkerHSFuelOilRME.setText("9080");
//        totalROBBunkerDieselOilDMA.setText("9090");
//        totalROBBunkerDieselOilDMB.setText("9100");
//        totalROBBunkerMESystemOil.setText("9110");
//        totalROBBunkerMECylinderOil.setText("91101");
//        totalROBBunkerAESystemOil.setText("91102");
//        totalROBBunkerFreshWater.setText("9200");
//        totalROBBunkerBallastWater.setText("9300");
//        draftForward.setText("89");
//        draftAft.setText("90");
    }

    public void sendReport(View view) {
        if (portName.isValid()
            && totalROBBunkerLSFuelOilRMD.isValid()
            && totalROBBunkerHSFuelOilRME.isValid()
            && totalROBBunkerDieselOilDMA.isValid()
            && totalROBBunkerDieselOilDMB.isValid()
            && totalROBBunkerMESystemOil.isValid()
            && totalROBBunkerMECylinderOil.isValid()
            && totalROBBunkerAESystemOil.isValid()
            && totalROBBunkerFreshWater.isValid()
            && totalROBBunkerBallastWater.isValid()
            && draftForward.isValid()
            && draftAft.isValid()) {

            shared.sendPayload(this, "INT&WAP"
                + "&" + portName.FIELD_CODE + "=" + portName.getValue()
                + "&" + etOperationsCommenceDate.FIELD_CODE + "=" + etOperationsCommenceDate.getValue()
                + "&" + totalROBBunkerLSFuelOilRMD.FIELD_CODE + "=" + totalROBBunkerLSFuelOilRMD.getValue()
                + "&" + totalROBBunkerHSFuelOilRME.FIELD_CODE + "=" + totalROBBunkerHSFuelOilRME.getValue()
                + "&" + totalROBBunkerDieselOilDMA.FIELD_CODE + "=" + totalROBBunkerDieselOilDMA.getValue()
                + "&" + totalROBBunkerDieselOilDMB.FIELD_CODE + "=" + totalROBBunkerDieselOilDMB.getValue()
                + "&" + totalROBBunkerMESystemOil.FIELD_CODE + "=" + totalROBBunkerMESystemOil.getValue()
                + "&" + totalROBBunkerMECylinderOil.FIELD_CODE + "=" + totalROBBunkerMECylinderOil.getValue()
                + "&" + totalROBBunkerAESystemOil.FIELD_CODE + "=" + totalROBBunkerAESystemOil.getValue()
                + "&" + totalROBBunkerFreshWater.FIELD_CODE + "=" + totalROBBunkerFreshWater.getValue()
                + "&" + totalROBBunkerBallastWater.FIELD_CODE + "=" + totalROBBunkerBallastWater.getValue()
                + "&" + draftForward.FIELD_CODE + "=" + draftForward.getValue()
                + "&" + draftAft.FIELD_CODE + "=" + draftAft.getValue()
                + "&");
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) { }

    @Override
    public void onBackPressed() {
        if (isDirty())
            shared.activityOnBackPressed();
        else
            finish();
    }

    boolean isDirty() {
        return portName.isDirty()
            || etOperationsCommenceDate.isDirty()
            || totalROBBunkerLSFuelOilRMD.isDirty()
            || totalROBBunkerHSFuelOilRME.isDirty()
            || totalROBBunkerDieselOilDMA.isDirty()
            || totalROBBunkerDieselOilDMB.isDirty()
            || totalROBBunkerMESystemOil.isDirty()
            || totalROBBunkerMECylinderOil.isDirty()
            || totalROBBunkerAESystemOil.isDirty()
            || totalROBBunkerFreshWater.isDirty()
            || totalROBBunkerBallastWater.isDirty()
            || draftForward.isDirty()
            || draftAft.isDirty();
    }
}