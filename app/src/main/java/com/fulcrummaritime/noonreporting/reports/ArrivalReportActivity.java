
package com.fulcrummaritime.noonreporting.reports;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.fulcrummaritime.noonreporting.items.AnchoringDateTime;
import com.fulcrummaritime.noonreporting.items.AverageRPM;
import com.fulcrummaritime.noonreporting.items.AverageSpeed;
import com.fulcrummaritime.noonreporting.BaseActivity;
import com.fulcrummaritime.noonreporting.items.DistanceMadeGood;
import com.fulcrummaritime.noonreporting.items.DraftAft;
import com.fulcrummaritime.noonreporting.items.DraftForward;
import com.fulcrummaritime.noonreporting.items.EndOfPassage;
import com.fulcrummaritime.noonreporting.items.EstimatedBerthingTime;
import com.fulcrummaritime.noonreporting.items.NoticeOfReadiness;
import com.fulcrummaritime.noonreporting.items.PortName;
import com.fulcrummaritime.noonreporting.R;
import com.fulcrummaritime.noonreporting.Shared;

import layout.TimePickerWithTitleFragment;

import com.fulcrummaritime.noonreporting.items.TotalConsumptionAESystemOil;
import com.fulcrummaritime.noonreporting.items.TotalConsumptionDieselOilDMA;
import com.fulcrummaritime.noonreporting.items.TotalConsumptionDieselOilDMB;
import com.fulcrummaritime.noonreporting.items.TotalConsumptionFreshWater;
import com.fulcrummaritime.noonreporting.items.TotalConsumptionHSFuelOil;
import com.fulcrummaritime.noonreporting.items.TotalConsumptionLSFuelOil;
import com.fulcrummaritime.noonreporting.items.TotalConsumptionMECylinderOil;
import com.fulcrummaritime.noonreporting.items.TotalConsumptionMESystemOil;
import com.fulcrummaritime.noonreporting.items.TotalDistanceMadeGood;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerAESystemOil;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerDieselOilDMA;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerDieselOilDMB;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerFreshWater;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerHSFuelOilRME;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerLSFuelOilRMD;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerMECylinderOil;
import com.fulcrummaritime.noonreporting.items.TotalROBBunkerMESystemOil;
import com.fulcrummaritime.noonreporting.items.VoyageAverageRPM;
import com.fulcrummaritime.noonreporting.items.VoyageAverageSlip;
import com.fulcrummaritime.noonreporting.items.VoyageAverageSpeed;
import com.fulcrummaritime.noonreporting.items.VoyageSlip;

import layout.DateAMPMPickerWithTitleFragment;
import layout.DateTimePickerWithTitleFragment;
import layout.EditTextWithTitleFragment;
import layout.ReportDividerFragment;
import layout.SendReportFragment;

public class ArrivalReportActivity extends BaseActivity implements ReportDividerFragment.OnFragmentInteractionListener,
        TimePickerWithTitleFragment.OnFragmentInteractionListener, DateTimePickerWithTitleFragment.OnFragmentInteractionListener, SendReportFragment.OnFragmentInteractionListener,
        EditTextWithTitleFragment.OnFragmentInteractionListener, DateAMPMPickerWithTitleFragment.OnFragmentInteractionListener {

    final Shared shared = new Shared(this);
    PortName portName;
    EndOfPassage endOfPassage;
    NoticeOfReadiness noticeOfReadiness;
    DistanceMadeGood distanceMadeGood;
    TotalDistanceMadeGood totalDistanceMadeGood;
    AverageSpeed averageSpeed;
    VoyageAverageSpeed voyageAverageSpeed;
    VoyageSlip voyageSlip;
    VoyageAverageSlip voyageAverageSlip;
    AverageRPM averageRPM;
    VoyageAverageRPM voyageAverageRPM;
    TotalConsumptionLSFuelOil totalConsumptionLSFuelOil;
    TotalConsumptionHSFuelOil totalConsumptionHSFuelOil;
    TotalConsumptionDieselOilDMA totalConsumptionDieselOilDMA;
    TotalConsumptionDieselOilDMB totalConsumptionDieselOilDMB;
    TotalConsumptionMESystemOil totalConsumptionMESystemOil;
    TotalConsumptionMECylinderOil totalConsumptionMECylinderOil;
    TotalConsumptionAESystemOil totalConsumptionAESystemOil;
    TotalConsumptionFreshWater totalConsumptionFreshWater;
    TotalROBBunkerLSFuelOilRMD totalROBBunkerLSFuelOilRMD;
    TotalROBBunkerHSFuelOilRME totalROBBunkerHSFuelOilRME;
    TotalROBBunkerDieselOilDMA totalROBBunkerDieselOilDMA;
    TotalROBBunkerDieselOilDMB totalROBBunkerDieselOilDMB;
    TotalROBBunkerMESystemOil totalROBBunkerMESystemOil;
    TotalROBBunkerMECylinderOil totalROBBunkerMECylinderOil;
    TotalROBBunkerAESystemOil totalROBBunkerAESystemOil;
    TotalROBBunkerFreshWater totalROBBunkerFreshWater;
    DraftForward draftForward;
    DraftAft draftAft;
    AnchoringDateTime anchoringDateTime;
    EstimatedBerthingTime estimatedBerthingTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.arrival_activity_menu_title);
        setContentView(R.layout.activity_arrival_report);

        portName = new PortName(this, fragmentManager.findFragmentById(R.id.portNameFragment));
        endOfPassage = new EndOfPassage(this, fragmentManager.findFragmentById(R.id.endOfPassageFragment));
        noticeOfReadiness = new NoticeOfReadiness(this, fragmentManager.findFragmentById(R.id.noticeOfReadinessFragment));
        distanceMadeGood = new DistanceMadeGood(this, fragmentManager.findFragmentById(R.id.distanceMadeGoodFragment));
        totalDistanceMadeGood = new TotalDistanceMadeGood(this, fragmentManager.findFragmentById(R.id.totalDistanceMadeGoodFragment));
        averageSpeed = new AverageSpeed(this, fragmentManager.findFragmentById(R.id.averageSpeedFragment));
        voyageAverageSpeed = new VoyageAverageSpeed(this, fragmentManager.findFragmentById(R.id.voyageAverageSpeedFragment));
        voyageSlip = new VoyageSlip(this, fragmentManager.findFragmentById(R.id.voyageSlipFragment));
        voyageAverageSlip = new VoyageAverageSlip(this, fragmentManager.findFragmentById(R.id.voyageAverageSlipFragment));
        averageRPM = new AverageRPM(this, fragmentManager.findFragmentById(R.id.averageRPMFragment));
        voyageAverageRPM = new VoyageAverageRPM(this, fragmentManager.findFragmentById(R.id.voyageAverageRPMFragment));
        totalConsumptionLSFuelOil = new TotalConsumptionLSFuelOil(this, fragmentManager.findFragmentById(R.id.totalConsumptionLSFuelOilFragment));
        totalConsumptionHSFuelOil = new TotalConsumptionHSFuelOil(this, fragmentManager.findFragmentById(R.id.totalConsumptionHSFuelOilFragment));
        totalConsumptionDieselOilDMA = new TotalConsumptionDieselOilDMA(this, fragmentManager.findFragmentById(R.id.totalConsumptionDieselOilDMAFragment));
        totalConsumptionDieselOilDMB = new TotalConsumptionDieselOilDMB(this, fragmentManager.findFragmentById(R.id.totalConsumptionDieselOilDMBFragment));
        totalConsumptionMESystemOil = new TotalConsumptionMESystemOil(this, fragmentManager.findFragmentById(R.id.totalConsumptionMESystemOilFragment));
        totalConsumptionMECylinderOil = new TotalConsumptionMECylinderOil(this, fragmentManager.findFragmentById(R.id.totalConsumptionMECylinderOilFragment));
        totalConsumptionAESystemOil = new TotalConsumptionAESystemOil(this, fragmentManager.findFragmentById(R.id.totalConsumptionAESystemOilFragment));
        totalConsumptionFreshWater = new TotalConsumptionFreshWater(this, fragmentManager.findFragmentById(R.id.totalConsumptionFreshWaterFragment));
        totalROBBunkerLSFuelOilRMD = new TotalROBBunkerLSFuelOilRMD(this, fragmentManager.findFragmentById(R.id.totalROBBunkerLSFuelOilRMDFragment));
        totalROBBunkerHSFuelOilRME = new TotalROBBunkerHSFuelOilRME(this, fragmentManager.findFragmentById(R.id.totalROBBunkerHSFuelOilRMEFragment));
        totalROBBunkerDieselOilDMA = new TotalROBBunkerDieselOilDMA(this, fragmentManager.findFragmentById(R.id.totalROBBunkerDieselOilDMAFragment));
        totalROBBunkerDieselOilDMB = new TotalROBBunkerDieselOilDMB(this, fragmentManager.findFragmentById(R.id.totalROBBunkerDieselOilDMBFragment));
        totalROBBunkerMESystemOil = new TotalROBBunkerMESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerMESystemOilFragment));
        totalROBBunkerMECylinderOil = new TotalROBBunkerMECylinderOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerMECylinderOilFragment));
        totalROBBunkerAESystemOil = new TotalROBBunkerAESystemOil(this, fragmentManager.findFragmentById(R.id.totalROBBunkerAESystemOilFragment));
        totalROBBunkerFreshWater = new TotalROBBunkerFreshWater(this, fragmentManager.findFragmentById(R.id.totalROBBunkerFreshWaterFragment));
        draftForward = new DraftForward(this, fragmentManager.findFragmentById(R.id.draftForwardFragment));
        draftAft = new DraftAft(this, fragmentManager.findFragmentById(R.id.draftAftFragment));
        anchoringDateTime = new AnchoringDateTime(this, fragmentManager.findFragmentById(R.id.anchoringDateTimeFragment));
        estimatedBerthingTime = new EstimatedBerthingTime(this, fragmentManager.findFragmentById(R.id.estimatedBerthingTimeFragment));

        // Pre-populate for testing.
//        portName.setText("Port of Romford");
//        distanceMadeGood.setText("10");
//        totalDistanceMadeGood.setText("20");
//        averageSpeed.setText("30");
//        voyageAverageSpeed.setText("40");
//        voyageSlip.setText("+9923");
//        voyageAverageSlip.setText("-0114");
//        averageRPM.setText("89");
//        voyageAverageRPM.setText("99");
//        totalConsumptionLSFuelOil.setText("9000");
//        totalConsumptionHSFuelOil.setText("9010");
//        totalConsumptionDieselOilDMA.setText("9020");
//        totalConsumptionDieselOilDMB.setText("9030");
//        totalConsumptionMESystemOil.setText("9040");
//        totalConsumptionMECylinderOil.setText("90501");
//        totalConsumptionAESystemOil.setText("90502");
//        totalConsumptionFreshWater.setText("9060");
//        totalROBBunkerLSFuelOilRMD.setText("9070");
//        totalROBBunkerHSFuelOilRME.setText("9080");
//        totalROBBunkerDieselOilDMA.setText("9090");
//        totalROBBunkerDieselOilDMB.setText("9100");
//        totalROBBunkerMESystemOil.setText("9110");
//        totalROBBunkerMECylinderOil.setText("91101");
//        totalROBBunkerAESystemOil.setText("91102");
//        totalROBBunkerFreshWater.setText("9200");
//        draftForward.setText("89");
//        draftAft.setText("90");
    }

    public void sendReport(View view) {
        if (portName.isValid()
            && distanceMadeGood.isValid()
            && totalDistanceMadeGood.isValid()
            && averageSpeed.isValid()
            && voyageAverageSpeed.isValid()
            && voyageSlip.isValid()
            && voyageAverageSlip.isValid()
            && averageRPM.isValid()
            && voyageAverageRPM.isValid()
            && totalConsumptionLSFuelOil.isValid()
            && totalConsumptionHSFuelOil.isValid()
            && totalConsumptionDieselOilDMA.isValid()
            && totalConsumptionDieselOilDMB.isValid()
            && totalConsumptionMESystemOil.isValid()
            && totalConsumptionMECylinderOil.isValid()
            && totalConsumptionAESystemOil.isValid()
            && totalConsumptionFreshWater.isValid()
            && totalROBBunkerLSFuelOilRMD.isValid()
            && totalROBBunkerHSFuelOilRME.isValid()
            && totalROBBunkerDieselOilDMA.isValid()
            && totalROBBunkerDieselOilDMB.isValid()
            && totalROBBunkerMESystemOil.isValid()
            && totalROBBunkerMECylinderOil.isValid()
            && totalROBBunkerAESystemOil.isValid()
            && totalROBBunkerFreshWater.isValid()
            && draftForward.isValid()
            && draftAft.isValid()) {

            shared.sendPayload(this, "INT&ARR"
                + "&" + portName.FIELD_CODE + "=" + portName.getValue()
                + "&" + endOfPassage.FIELD_CODE + "=" + endOfPassage.getValue()
                + "&" + noticeOfReadiness.FIELD_CODE + "=" + noticeOfReadiness.getValue()
                + "&" + distanceMadeGood.FIELD_CODE + "=" + distanceMadeGood.getValue()
                + "&" + totalDistanceMadeGood.FIELD_CODE + "=" + totalDistanceMadeGood.getValue()
                + "&" + averageSpeed.FIELD_CODE + "=" + averageSpeed.getValue()
                + "&" + voyageAverageSpeed.FIELD_CODE + "=" + voyageAverageSpeed.getValue()
                + "&" + voyageSlip.FIELD_CODE + "=" + voyageSlip.getValue()
                + "&" + voyageAverageSlip.FIELD_CODE + "=" + voyageAverageSlip.getValue()
                + "&" + averageRPM.FIELD_CODE + "=" + averageRPM.getValue()
                + "&" + voyageAverageRPM.FIELD_CODE + "=" + voyageAverageRPM.getValue()
                + "&" + totalConsumptionLSFuelOil.FIELD_CODE + "=" + totalConsumptionLSFuelOil.getValue()
                + "&" + totalConsumptionHSFuelOil.FIELD_CODE + "=" + totalConsumptionHSFuelOil.getValue()
                + "&" + totalConsumptionDieselOilDMA.FIELD_CODE + "=" + totalConsumptionDieselOilDMA.getValue()
                + "&" + totalConsumptionDieselOilDMB.FIELD_CODE + "=" + totalConsumptionDieselOilDMB.getValue()
                + "&" + totalConsumptionMESystemOil.FIELD_CODE + "=" + totalConsumptionMESystemOil.getValue()
                + "&" + totalConsumptionMECylinderOil.FIELD_CODE + "=" + totalConsumptionMECylinderOil.getValue()
                + "&" + totalConsumptionAESystemOil.FIELD_CODE + "=" + totalConsumptionAESystemOil.getValue()
                + "&" + totalConsumptionFreshWater.FIELD_CODE + "=" + totalConsumptionFreshWater.getValue()
                + "&" + totalROBBunkerLSFuelOilRMD.FIELD_CODE + "=" + totalROBBunkerLSFuelOilRMD.getValue()
                + "&" + totalROBBunkerHSFuelOilRME.FIELD_CODE + "=" + totalROBBunkerHSFuelOilRME.getValue()
                + "&" + totalROBBunkerDieselOilDMA.FIELD_CODE + "=" + totalROBBunkerDieselOilDMA.getValue()
                + "&" + totalROBBunkerDieselOilDMB.FIELD_CODE + "=" + totalROBBunkerDieselOilDMB.getValue()
                + "&" + totalROBBunkerMESystemOil.FIELD_CODE + "=" + totalROBBunkerMESystemOil.getValue()
                + "&" + totalROBBunkerMECylinderOil.FIELD_CODE + "=" + totalROBBunkerMECylinderOil.getValue()
                + "&" + totalROBBunkerAESystemOil.FIELD_CODE + "=" + totalROBBunkerAESystemOil.getValue()
                + "&" + totalROBBunkerFreshWater.FIELD_CODE + "=" + totalROBBunkerFreshWater.getValue()
                + "&" + draftForward.FIELD_CODE + "=" + draftForward.getValue()
                + "&" + draftAft.FIELD_CODE + "=" + draftAft.getValue()
                + "&" + anchoringDateTime.FIELD_CODE + "=" + anchoringDateTime.getValue()
                + "&" + estimatedBerthingTime.FIELD_CODE + "=" + estimatedBerthingTime.getValue()
                + "&");
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) { }

    @Override
    public void onBackPressed() {
        if (isDirty())
            shared.activityOnBackPressed();
        else
            finish();
    }

    boolean isDirty() {
        return portName.isDirty()
            || endOfPassage.isDirty()
            || noticeOfReadiness.isDirty()
            || distanceMadeGood.isDirty()
            || totalDistanceMadeGood.isDirty()
            || averageSpeed.isDirty()
            || voyageAverageSpeed.isDirty()
            || voyageSlip.isDirty()
            || voyageAverageSlip.isDirty()
            || averageRPM.isDirty()
            || voyageAverageRPM.isDirty()
            || totalConsumptionLSFuelOil.isDirty()
            || totalConsumptionHSFuelOil.isDirty()
            || totalConsumptionDieselOilDMA.isDirty()
            || totalConsumptionDieselOilDMB.isDirty()
            || totalConsumptionMESystemOil.isDirty()
            || totalConsumptionMECylinderOil.isDirty()
            || totalConsumptionAESystemOil.isDirty()
            || totalConsumptionFreshWater.isDirty()
            || totalROBBunkerLSFuelOilRMD.isDirty()
            || totalROBBunkerHSFuelOilRME.isDirty()
            || totalROBBunkerDieselOilDMA.isDirty()
            || totalROBBunkerDieselOilDMB.isDirty()
            || totalROBBunkerMESystemOil.isDirty()
            || totalROBBunkerMECylinderOil.isDirty()
            || totalROBBunkerAESystemOil.isDirty()
            || totalROBBunkerFreshWater.isDirty()
            || draftForward.isDirty()
            || draftAft.isDirty()
            || anchoringDateTime.isDirty()
            || estimatedBerthingTime.isDirty();
    }
}
