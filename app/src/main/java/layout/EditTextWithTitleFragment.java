
package layout;

import android.content.Context;

import android.net.Uri;

import android.os.Bundle;

import android.support.v4.app.Fragment;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.TextView;

import com.fulcrummaritime.noonreporting.R;

public class EditTextWithTitleFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView editTextLabel;
    private EditText editText;

    public static EditTextWithTitleFragment newInstance(android.support.v4.app.Fragment fragment, String labelText, int inputType, int width, int maxLength, String digitsAllowed, String hint) {
        EditTextWithTitleFragment f = (EditTextWithTitleFragment)fragment;
        f.setLabelText(labelText);
        f.setInputType(inputType);
        f.setWidth(width);
        f.setMaxLength(maxLength);

        // if (digitsAllowed != null)
        //     f.setKeyListener(digitsAllowed);

        if (hint != null)
            f.setHint(hint);

        // f.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        return f;
    }

    public static EditTextWithTitleFragment newInstance(android.support.v4.app.Fragment fragment) {
        return (EditTextWithTitleFragment)fragment;
    }

    public EditTextWithTitleFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_text_with_title, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTextLabel = (TextView)view.findViewById(R.id.editTextLabel);
        editText = (EditText)view.findViewById(R.id.editText);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // External object api.
    public void setLabelText(String labelText) {
        editTextLabel.setText(labelText);
    }

    public void setInputType(int inputType) {
        editText.setInputType(inputType);
        editText.setRawInputType(inputType);
        Log.i("editText.getInputType()", editText.getInputType() + "");
        if (inputType == InputType.TYPE_CLASS_NUMBER) {
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus && editText.getText().length() > 0)
                        editText.setText(String.valueOf(Integer.valueOf(editText.getText().toString())));
                }
            });

            editText.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    if (s.toString().startsWith("0"))
                        s.replace(0, s.length(), s.subSequence(1, s.length()));
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });
        }
    }

    public void setWidth(int width) {
        editText.getLayoutParams().width = width;
    }

    public void setMaxLength(int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(FilterArray);
    }

    public void setHint(String hint) {
        editText.setHint(hint);
    }

    public void setKeyListener(String digits) {
        editText.setKeyListener(DigitsKeyListener.getInstance(digits));
    }

    public void addTextChangedListener(TextWatcher watcher) {
        editText.addTextChangedListener(watcher);
    }

    public void setText(String text) {
        editText.setText(text);
    }

    public boolean isValid(String errorText) {
        if (editText.getText().length() == 0) {
            editText.setError(errorText);
            editText.requestFocus();
            return false;
        }
        return true;
    }

    public void setError(String errTxt) {
        editText.setError(errTxt);
        editText.requestFocus();
    }

    public String getValueAsString() {
        return editText.getText().toString();
    }

    public boolean isDirty() {
        return editText.getText().length() > 0;
    }

    public void setMultiLine(int minLines, int maxLines) {
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        editText.setSingleLine(false);
        editText.setMinLines(minLines);
        editText.setMaxLines(maxLines);
        editText.setHorizontallyScrolling(false);
        editText.setGravity(Gravity.TOP | Gravity.LEFT);
    }

    public void setGravity(int gravity) {
        editText.setGravity(gravity);
    }

    /*
    public void setImeOptions(int imeOptions) {
        editText.setImeOptions(imeOptions);
    }

    public void setNextFocusRight(int nextFocusRightId) {
        editText.setNextFocusRightId(nextFocusRightId);
        editText.setNextFocusDownId(nextFocusRightId);
        editText.setNextFocusForwardId(nextFocusRightId);
        editText.setNextFocusLeftId(nextFocusRightId);
        editText.setNextFocusUpId(nextFocusRightId);
    }

    // public void setNextFocusDownId(int nextFocusRightId) {
    //     editText.setNextFocusDownId(nextFocusRightId);
    // }
    */
}