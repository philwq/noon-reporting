
package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fulcrummaritime.noonreporting.R;

import java.util.Calendar;

public class TimeTimeDatePickerWithTitleFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView timeTimeDatePickerLabel;
    private TimePicker timeTimeDatePickerTime;
    private TimePicker timeTimeDatePickerTimeOne;
    private DatePicker timeTimeDatePickerDate;

    public static TimeTimeDatePickerWithTitleFragment newInstance(android.support.v4.app.Fragment fragment) {
        return (TimeTimeDatePickerWithTitleFragment)fragment;
    }

    public TimeTimeDatePickerWithTitleFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_time_time_date_picker_with_title, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        timeTimeDatePickerLabel = (TextView)view.findViewById(R.id.timeTimeDatePickerLabel);

        timeTimeDatePickerTime = (TimePicker)view.findViewById(R.id.timeTimeDatePickerTime);
        timeTimeDatePickerTime.setIs24HourView(true);
        timeTimeDatePickerTime.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

        timeTimeDatePickerTimeOne = (TimePicker)view.findViewById(R.id.timeTimeDatePickerTimeOne);
        timeTimeDatePickerTimeOne.setIs24HourView(true);
        timeTimeDatePickerTimeOne.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

        timeTimeDatePickerDate = (DatePicker)view.findViewById(R.id.timeTimeDatePickerDate);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // External object api.
    public void setLabelText(String labelText) {
        timeTimeDatePickerLabel.setText(labelText);
    }

    public String getValue() {
        String hur = "0" + timeTimeDatePickerTime.getCurrentHour();
        String min = "0" + timeTimeDatePickerTime.getCurrentMinute();
        String val = hur.substring(hur.length()-2) + min.substring(min.length() - 2);
        hur = "0" + timeTimeDatePickerTimeOne.getCurrentHour();
        min = "0" + timeTimeDatePickerTimeOne.getCurrentMinute();
        val += hur.substring(hur.length()-2) + min.substring(min.length()-2);
        String mth = "0" + (timeTimeDatePickerDate.getMonth()+1);
        String day = "0" + timeTimeDatePickerDate.getDayOfMonth();
        return val + timeTimeDatePickerDate.getYear() + mth.substring(mth.length()-2) + day.substring(day.length()-2);
    }
}
