
package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.fulcrummaritime.noonreporting.R;

import java.util.Calendar;

public class DateAMPMPickerWithTitleFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView dateAMPMPickerLabel;
    private DatePicker dateAMPMPickerDate;
    private ToggleButton dateAMPMPickerToggle;

    public static DateAMPMPickerWithTitleFragment newInstance(android.support.v4.app.Fragment fragment, String labelText) {
        DateAMPMPickerWithTitleFragment f = (DateAMPMPickerWithTitleFragment)fragment;
        f.setLabelText(labelText);
        return f;
    }

    public static DateAMPMPickerWithTitleFragment newInstance(android.support.v4.app.Fragment fragment) {
        return (DateAMPMPickerWithTitleFragment)fragment;
    }

    public DateAMPMPickerWithTitleFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_date_ampm_picker_with_title, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dateAMPMPickerLabel = (TextView)view.findViewById(R.id.dateAMPMPickerLabel);
        dateAMPMPickerDate = (DatePicker)view.findViewById(R.id.dateAMPMPickerDate);
        dateAMPMPickerToggle = (ToggleButton)view.findViewById(R.id.dateAMPMPickerToggle);
        dateAMPMPickerToggle.setChecked(Calendar.getInstance().get(Calendar.HOUR_OF_DAY) > 11);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // External object api.
    public void setLabelText(String labelText) {
        dateAMPMPickerLabel.setText(labelText);
    }

    public String getDateAsYYYYMMDD() {
        String mth = "0" + (dateAMPMPickerDate.getMonth()+1);
        String day = "0" + dateAMPMPickerDate.getDayOfMonth();
        return dateAMPMPickerDate.getYear() + mth.substring(mth.length()-2) + day.substring(day.length()-2);
    }

    public String getAMPM() {
        return dateAMPMPickerToggle.getText().toString();
    }

    /*
    public boolean isValid(String errorText) {
        if (dateAMPMPickerToggle.getText().equals("Not Set"))
        if (editText.getText().length() == 0) {
            editText.setError(errorText);
            editText.requestFocus();
            return false;
        }
        return true;
    }
    */
}
