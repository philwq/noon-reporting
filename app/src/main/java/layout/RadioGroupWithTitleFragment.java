
package layout;

import android.content.Context;

import android.net.Uri;

import android.os.Bundle;

import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.fulcrummaritime.noonreporting.R;

public class RadioGroupWithTitleFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView editTextLabel;
    RadioGroup radioGroup00;
    public final RadioButton[] radioButtons = new RadioButton[6];

    public static RadioGroupWithTitleFragment newInstance(android.support.v4.app.Fragment fragment) {
        return (RadioGroupWithTitleFragment)fragment;
    }

    public RadioGroupWithTitleFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_radio_group, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTextLabel = (TextView)view.findViewById(R.id.editTextLabel);

        RadioGroup radioGroup00 = (RadioGroup)view.findViewById(R.id.radioGroup00);
        radioGroup00.setOrientation(LinearLayout.VERTICAL);

        radioButtons[0] = (RadioButton)view.findViewById(R.id.radioButton00);
        radioButtons[1] = (RadioButton)view.findViewById(R.id.radioButton01);
        radioButtons[2] = (RadioButton)view.findViewById(R.id.radioButton02);
        radioButtons[3] = (RadioButton)view.findViewById(R.id.radioButton03);
        radioButtons[4] = (RadioButton)view.findViewById(R.id.radioButton04);
        radioButtons[5] = (RadioButton)view.findViewById(R.id.radioButton05);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // External object api.
    public void setLabelText(String labelText) {
        editTextLabel.setText(labelText);
    }

    public boolean isValid(String errorText) {
        for (int i=0; i<radioButtons.length; i++) {
            if (radioButtons[i].isChecked())
                return true;
        }
        radioButtons[0].setError(errorText);
        radioButtons[0].requestFocus();
        return false;
    }

    public String getValue() {
        for (int i=0; i<radioButtons.length; i++) {
            if (radioButtons[i].isChecked())
                return radioButtons[i].getText().toString();
        }
        return "";
    }

    public void clearError() {
        for (int i=0; i<radioButtons.length; i++)
            radioButtons[i].setError(null);
    }

    public void initialiseRadioButton(int ix, String text) {
        radioButtons[ix].setText(text);
        radioButtons[ix].setVisibility(View.VISIBLE);
    }

    public void setEnabled(boolean enabled) {
        if (!enabled)
            setChecked(enabled);

        for (int i=0; i<radioButtons.length; i++)
            radioButtons[i].setEnabled(enabled);
    }

    public void setChecked(boolean checked) {
        for (int i=0; i<radioButtons.length; i++)
            radioButtons[i].setChecked(checked);
    }
}