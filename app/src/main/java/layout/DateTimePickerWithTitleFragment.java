
package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fulcrummaritime.noonreporting.R;

import java.util.Calendar;

public class DateTimePickerWithTitleFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView timePickerLabel;
    private DatePicker timePickerDate;
    private TimePicker timePickerTime;

    public static DateTimePickerWithTitleFragment newInstance(android.support.v4.app.Fragment fragment, String labelText) {
        DateTimePickerWithTitleFragment f = (DateTimePickerWithTitleFragment)fragment;
        f.setLabelText(labelText);
        return f;
    }

    public static DateTimePickerWithTitleFragment newInstance(android.support.v4.app.Fragment fragment) {
        return (DateTimePickerWithTitleFragment)fragment;
    }

    public DateTimePickerWithTitleFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_date_time_picker_with_title, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        timePickerLabel = (TextView)view.findViewById(R.id.dateTimePickerLabel);
        timePickerDate = (DatePicker)view.findViewById(R.id.dateTimePickerDate);
        timePickerTime = (TimePicker)view.findViewById(R.id.dateTimePickerTime);
        timePickerTime.setIs24HourView(true);
        timePickerTime.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // External object api.
    public void setLabelText(String labelText) {
        timePickerLabel.setText(labelText);
    }

    public String getTimeAsHHMM() {
        String hur = "0" + timePickerTime.getCurrentHour();
        String min = "0" + timePickerTime.getCurrentMinute();
        return hur.substring(hur.length()-2) + min.substring(min.length()-2);
    }

    public String getDateAsYYYYMMDD() {
        String mth = "0" + (timePickerDate.getMonth()+1);
        String day = "0" + timePickerDate.getDayOfMonth();
        return timePickerDate.getYear() + mth.substring(mth.length()-2) + day.substring(day.length()-2);
    }

    public int getTimePickerDateId() {
        return timePickerDate.getId();
    }
}
