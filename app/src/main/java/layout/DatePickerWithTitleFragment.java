
package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.DatePicker;
import android.widget.TextView;

import com.fulcrummaritime.noonreporting.R;

public class DatePickerWithTitleFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView datePickerLabel;
    private DatePicker datePickerDate;

    public static DatePickerWithTitleFragment newInstance(android.support.v4.app.Fragment fragment) {
        return (DatePickerWithTitleFragment)fragment;
    }

    public DatePickerWithTitleFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_date_picker_with_title, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        datePickerLabel = (TextView)view.findViewById(R.id.datePickerLabel);
        datePickerDate = (DatePicker)view.findViewById(R.id.datePickerDate);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // External object api.
    public void setLabelText(String labelText) {
        datePickerLabel.setText(labelText);
    }

    public String getDateAsYYYYMMDD() {
        String mth = "0" + (datePickerDate.getMonth()+1);
        String day = "0" + datePickerDate.getDayOfMonth();
        return datePickerDate.getYear() + mth.substring(mth.length()-2) + day.substring(day.length()-2);
    }
}