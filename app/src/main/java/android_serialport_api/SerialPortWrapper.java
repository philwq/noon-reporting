/*
 * Copyright 2009 Cedric Priscal
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package android_serialport_api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;

import android.util.Log;

import com.fulcrummaritime.noonreporting.R;

public class SerialPortWrapper {

    public int errorId;
    private String device;
    private SerialPort mSerialPort;
    private OutputStream mOutputStream;
    private InputStream mInputStream;
    private ReadThread mReadThread;

    public SerialPortWrapper(String device) {
        this.device = device;
    }

    private SerialPort openSerialPort() throws SecurityException, IOException, InvalidParameterException {

        if (mSerialPort == null) {
            String path = device; // sp.getString("DEVICE", "");
            int baudrate = 9600;

			/* Check parameters */
            if ( (path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

			/* Open the serial port */
            return new SerialPort(new File(path), baudrate, 0);
        }

        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null)
            mReadThread.interrupt();

        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    public void startReading() {
        try {
            mSerialPort = openSerialPort();
            mInputStream = mSerialPort.getInputStream();

		    /* Create a receiving thread */
            mReadThread = new ReadThread();
            mReadThread.start();

        } catch (SecurityException e) {
            errorId = R.string.error_security;
        } catch (IOException e) {
            errorId = R.string.error_unknown;
        } catch (InvalidParameterException e) {
            errorId = R.string.error_configuration;
        }
    }

    public boolean writeOnce(String payload) {
        try {
            mSerialPort = openSerialPort();
            mOutputStream = mSerialPort.getOutputStream();


            mOutputStream.write(payload.getBytes());
            mOutputStream.write('\r');
            mOutputStream.write('\n');
            mOutputStream.flush();

            closeSerialPort();

            return true;

        } catch (SecurityException e) {
            errorId = R.string.error_security;
        } catch (IOException e) {
            errorId = R.string.error_unknown;
        } catch (InvalidParameterException e) {
            errorId = R.string.error_configuration;
        }

        return false;
    }

    private class ReadThread extends Thread {

        @Override
        public void run() {
            super.run();
            StringBuffer lineBuffer = new StringBuffer();
            while(!isInterrupted()) {
                int size;
                try {
                    byte[] buffer = new byte[1024];
                    if (mInputStream == null) return;
                    size = mInputStream.read(buffer);
                    if (size > 0) {
                        // Log.d("ToHex:", String.format("%02X ", buffer[0]));
                        // Log.d("ToHex:", (buffer[size-1] & 0xFF) + "");
                        lineBuffer.append(new String(buffer, 0, size));
                        // Log.d("lineBuffer:", lineBuffer.toString());

                        // if ((buffer[size-1] & 0xFF) == 13) {
                        if ((buffer[size-1] & 0xFF) == 13 || (buffer[size-1] & 0xFF) == 10 || lineBuffer.toString().toLowerCase().endsWith("&end")) {
                            onDataReceived(lineBuffer.toString());
                            lineBuffer.setLength(0);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    protected void onDataReceived(String payload){};
}
